package results

type Resultado struct {
	Id      int64  `json:"id"`
	Mensaje string `json:"mensaje"`
}
type Resultados []Resultado
