package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"../models"
	"github.com/gorilla/mux"
)

// Listar Usos vehiculares
func ListarPreguntas(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	res := models.ListarPreguntas()
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarPregunta(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	clase := vars["clase"]
	res := models.ListarPregunta(clase)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
