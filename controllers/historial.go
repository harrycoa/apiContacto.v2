package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"../models"
	"github.com/gorilla/mux"
)

func HistorialContactoCont(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	contactol := models.ContactoHistorialCont()
	j, err := json.Marshal(contactol)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()

}
func HistorialContactoParametroCont(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	fecha1 := vars["fecha1"] //Esto ya es un String
	fecha2 := vars["fecha2"] //Esto ya es un String

	// fmt.Println("===============FECHA INICIAL=====================")
	// fmt.Println(fecha1)
	// fmt.Println("============FECHA FINAL========================")
	// fmt.Println(fecha2)

	res := models.ContactoHistorialParametroCont(fecha1, fecha2)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func HistorialContactoParametroPlaca(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	placa := vars["placa"] //Esto ya es un String

	// fmt.Println("===============FECHA INICIAL=====================")
	// fmt.Println(fecha1)
	// fmt.Println("============FECHA FINAL========================")
	// fmt.Println(fecha2)

	res := models.ContactoHistorialParametroPlaca(placa)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
