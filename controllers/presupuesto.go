package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
	"github.com/gorilla/mux"
)

// SelectContacto controlador de prueba 3
func InsertarPresupuesto(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	obj := models.Presupuesto{}
	err := json.NewDecoder(r.Body).Decode(&obj)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	b := models.CrearPresupuesto(
		obj.NumeroPresupuesto,
		obj.MontoPresupuesto,
		obj.FechaProgramada,
		obj.VozCliente,
		obj.MotivoAceptacion,
		obj.MotivoNoAceptacion,
		obj.IdMotivoResultado)
	fmt.Println(b)
	models.Closeconnection()
}

func ListarPresupuestos(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	presupuesl := models.ListarPresupuestos()
	j, err := json.Marshal(presupuesl)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarPresupuesto(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	numeroPresupuesto := vars["serie"] //Esto ya es un String
	res := models.ListarPresupuesto(numeroPresupuesto)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
