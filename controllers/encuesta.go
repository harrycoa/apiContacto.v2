package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
	"github.com/gorilla/mux"
)

// SelectContacto controlador de prueba 3
func InsertarEncuesta(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	encuesta := models.Encuesta{}
	err := json.NewDecoder(r.Body).Decode(&encuesta)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}

	b := models.CrearEncuesta(
		encuesta.EstadoEncuenta,
		encuesta.VozCliente,
		encuesta.Sugerencia,
		encuesta.IdServicio,
		encuesta.IdPregunta)
	fmt.Println(b)
	models.Closeconnection()
}

func ListarEncuestas(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	encuestal := models.ListarEncuestas()
	j, err := json.Marshal(encuestal)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarEncuesta(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	estadoEncuesta := vars["estadoEncuesta"] //Esto ya es un String
	res := models.ListarEncuesta(estadoEncuesta)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
