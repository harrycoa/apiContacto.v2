package controllers

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/rafalgolarz/timestamp"

	"../commons"
	"../configuration"
	"../models"
	"../models/user"
	"../results"
)

// UserCreate permite crear un usuario
func UserCreate(w http.ResponseWriter, r *http.Request) {

	newUser := user.Create{}
	m := models.Message{}

	// obtiene contenido del body
	err := json.NewDecoder(r.Body).Decode(&newUser)

	if err != nil {
		m.Message = fmt.Sprintf("Error al leer el usuario a registrar: %s", err)
		m.Code = http.StatusBadRequest
		commons.DisplayMessage(w, m)
		return
	}

	// comparar contraseñas
	if newUser.ClaveUsuario != newUser.ConfirmarClave {
		m.Message = "Las contraseñas no coinciden"
		m.Code = http.StatusBadRequest
		commons.DisplayMessage(w, m)
		return
	}

	db, err := configuration.OpenDB()
	checkErr(err)
	defer db.Close()

	/****************************VERIFICAR QUE NO EXISTA EL USUARIO****************************/
	// setear consulta
	existeUser, err := db.Prepare(`SELECT nombreUsuario FROM x_taUsuario WHERE nombreUsuario = ?`)
	checkErr(err)
	defer existeUser.Close()

	rows, err := existeUser.Query(newUser.NombreUsuario)
	checkErr(err)

	if rows.Next() {
		m := models.Message{
			Message: "El Usuario ya existe",
			Code:    http.StatusBadRequest,
		}
		commons.DisplayMessage(w, m)
		return
	}
	/********************************************************************/

	/*******************************INSERTAR USUARIO*************************************/
	// Prepare statement for inserting data (username, email, fullname, password)
	// insertUser, err := db.Prepare(`INSERT INTO user_d VALUES( ?, ?, ?, ? )`)
	queryInsertarUsuarioSQL := `INSERT x_taUsuario  
		SET nombreUsuario=?
			, claveUsuario=?
			, estado=?
			, dni=?
			, nombreCompleto=?
			, email=?
			, telefono=?
			, direccion=?
			, fechaRegistro=?
			, fechaModificacion=?	`
	insertUser, err := db.Prepare(queryInsertarUsuarioSQL)
	checkErr(err)
	defer insertUser.Close()

	insertarRolUsuarioSQL := `INSERT x_taRolUsuario SET nombreUsuario=?, idRolUsuario=?`
	insertRol, err := db.Prepare(insertarRolUsuarioSQL)
	checkErr(err)
	defer insertRol.Close()

	c := sha256.Sum256([]byte(newUser.ClaveUsuario))
	pwd := fmt.Sprintf("%x", c)

	estadoUsuario := 1 //usuario activo
	idRolUsuario := 1  //código rol por defecto
	fechaRegistro := timestamp.FromNow{}.String()
	fechaModificacion := timestamp.FromNow{}.String()

	res, err := insertUser.Exec(newUser.NombreUsuario, pwd,
		estadoUsuario, newUser.DNI, newUser.NombreCompleto, newUser.Email,
		newUser.Telefono, newUser.Direccion, fechaRegistro, fechaModificacion,
	)

	if err != nil {
		m.Message = fmt.Sprintf("Error al crear el registro: %s", err)
		m.Code = http.StatusBadRequest
		commons.DisplayMessage(w, m)
		return
	}
	id, err := res.LastInsertId()
	checkErr(err)

	res, err = insertRol.Exec(newUser.NombreUsuario, idRolUsuario)

	if err != nil {
		m.Message = fmt.Sprintf("Error al crear el rol de usuario: %s", err)
		m.Code = http.StatusBadRequest
		commons.DisplayMessage(w, m)
		return
	}
	mensaje := fmt.Sprintf("Usuario creado con éxito,id: %d", id)
	results.CreatedResult(w, mensaje)
}

func checkErr(err error) {
	if err != nil {
		panic(err.Error())
	}
}
