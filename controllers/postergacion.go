package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
)

// SelectContacto controlador de prueba 3
func InsertarPostergacion(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	obj := models.Postergacion{}
	err := json.NewDecoder(r.Body).Decode(&obj)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	b := models.CrearPostergacion(
		obj.FechaEntregaReprogramada,
		obj.MotivoAceptacion,
		obj.VozCliente,
		obj.MotivoCambioFecha,
		obj.IdMotivoResultado)
	fmt.Println(b)
	models.Closeconnection()
}
