package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"../commons"
	"../configuration"
	"../models"
)

// SelectEstado controlador de prueba 3
func InsertarFormularioEntrega(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	formularioentrega := models.FormularioEntrega{}
	vehiculo := models.Vehiculo{}
	vehiculoentrega := models.VehiculoEntrega{}
	err := json.NewDecoder(r.Body).Decode(&formularioentrega)
	if err != nil {

		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	vehiculo = formularioentrega.Vehiculo
	vehiculoentrega = formularioentrega.VehiculoEntrega
	db, err := configuration.OpenDB()
	checkErr(err)
	defer db.Close()
	/****************************VERIFICAR QUE NO EXISTA EL USUARIO****************************/
	// setear consulta
	existe, err := db.Prepare(`SELECT serie FROM x_tavehiculo WHERE serie = ?`)
	checkErr(err)
	defer existe.Close()

	rows, err := existe.Query(vehiculo.Serie)
	checkErr(err)

	if rows.Next() {
		m := models.Message{
			Message: "El vehículo con serie:" + vehiculo.Serie + " ya está registrado.",
			Code:    http.StatusPreconditionFailed,
		}
		commons.DisplayMessage(w, m)
		return
	}

	/********************************************************************/
	// CREAR VEHICULO
	a := models.CrearVehiculo(
		vehiculo.Serie,
		vehiculo.Placa,
		vehiculo.Motor,
		vehiculo.Chasis,
		vehiculo.Vin,
		vehiculo.AnioFabricacion,
		vehiculo.Modelo)
	// CREAR VEHICULO ENTREGA
	// b:=
	models.CrearVehiculoEntrega(
		vehiculoentrega.FechaProgramacionEntrega,
		vehiculoentrega.FechaEntregaVehiculo,
		vehiculoentrega.PersonaEntrega,
		vehiculoentrega.Telefono,
		vehiculoentrega.Celular,
		vehiculoentrega.OpinionEntrega,
		vehiculoentrega.FechaCitaReferencial2,
		a.IdVehiculo,
		vehiculoentrega.IdVehiculoUso,
		vehiculoentrega.IdDepartamento,
		vehiculoentrega.IdProvincia)
	models.Closeconnection()

	m := models.Message{Code: 201, Message: "Registro Exitoso!"}
	j, err := json.Marshal(m)
	if err != nil {
		log.Fatalf("Error al convertir el mensaje: %s", err)
	}
	w.WriteHeader(http.StatusCreated)
	w.Write(j)

}
