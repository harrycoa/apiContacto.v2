package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"time"

	"../models"
	"github.com/rafalgolarz/timestamp"
)

// InsertarFormulario
func InsertarFormulario(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	// OBJETOS VACIOS
	formulario := models.Formulario{}
	contacto := models.Contacto{}
	cita := []models.Cita{}
	nocita := models.NoCita{}
	vehiculo := models.Vehiculo{}
	// presupuesto := models.Presupuesto{}
	// ordenTrabajo := models.OrdenTrabajo{}
	// postergacion := models.Postergacion{}
	// encuesta := models.Encuesta{}
	// seguimiento := models.Seguimiento{}
	// recomendacion := models.Recomendacion{}

	// Pronosticador
	pronostico5k := models.Pronostico5k{}

	err := json.NewDecoder(r.Body).Decode(&formulario)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	// OBJETOS DENTRO DEL FORMULARIO
	contacto = formulario.Contacto
	cita = formulario.Cita
	nocita = formulario.NoCita
	vehiculo = formulario.Vehiculo
	// presupuesto = formulario.Presupuesto
	// ordenTrabajo = formulario.OrdenTrabajo
	// postergacion = formulario.Postergacion
	// encuesta = formulario.Encuesta
	// seguimiento = formulario.Seguimiento
	// recomendacion = formulario.Recomendacion

	// Pronosticador
	pronostico5k = formulario.Pronostico5k

	// CREAR NO CITA
	d := models.CrearNoCita(nocita.ClaseRazon,
		nocita.DescripcionNoCita,
		nocita.SabeKm,
		nocita.KmFaltaRecorrido,
		nocita.SabePromedio,
		nocita.PromedioRecorrido,
		nocita.UsoVehicular,
		nocita.FechaPropuesta,
		nocita.FechaSugeridaCliente,
		nocita.FechaCorregida,
		nocita.FechaFinal,
		nocita.FechaSiguienteLlamada,
		nocita.Concesionario,
		nocita.CausaRechazoCita,
		nocita.CausaRechazoCorasur,
		nocita.ObservacionesNoCita,
		nocita.Comentarios,
		nocita.IdRazonNoCita)

	// CREAR VEHICULO
	c := models.CrearVehiculo(
		vehiculo.Serie,
		vehiculo.Placa,
		vehiculo.Motor,
		vehiculo.Chasis,
		vehiculo.Vin,
		vehiculo.AnioFabricacion,
		vehiculo.Modelo)

	// CREAR PRESUPUESTO
	// e := models.CrearPresupuesto(
	// 	presupuesto.NumeroPresupuesto,
	// 	presupuesto.MontoPresupuesto,
	// 	presupuesto.FechaProgramada,
	// 	presupuesto.VozCliente,
	// 	presupuesto.MotivoAceptacion,
	// 	presupuesto.MotivoNoAceptacion,
	// 	presupuesto.IdMotivoResultado)

	// CREAR FINALIZACION TRABAJO
	// f := models.CrearOrdenTrabajo(
	// 	ordenTrabajo.FechaRecojo,
	// 	ordenTrabajo.Mensaje,
	// 	ordenTrabajo.FechaSolicitada,
	// 	ordenTrabajo.MotivoPostergacion,
	// 	ordenTrabajo.HoraSolicitada,
	// 	ordenTrabajo.IdMotivoResultado)

	// CREAR POSTERGACION
	// g := models.CrearPostergacion(
	// 	postergacion.FechaEntregaReprogramada,
	// 	postergacion.MotivoAceptacion,
	// 	postergacion.VozCliente,
	// 	postergacion.MotivoCambioFecha,
	// 	postergacion.IdMotivoResultado)

	// CREAR ENCUESTA
	// h := models.CrearEncuesta(
	// 	encuesta.EstadoEncuenta,
	// 	encuesta.VozCliente,
	// 	encuesta.Sugerencia,
	// 	encuesta.IdServicio,
	// 	encuesta.IdPregunta)
	// CREAR SEGUIMIENTO
	// i := models.CrearSeguimiento(
	// 	seguimiento.VozCliente,
	// 	seguimiento.AlertaTDP,
	// 	seguimiento.ResultadoAlerta,
	// 	seguimiento.AtencionAlerta,
	// 	seguimiento.DetalleAtencion,
	// 	seguimiento.FechaCitaRetorno,
	// 	seguimiento.TipoDescuento,
	// 	seguimiento.Caducidad,
	// 	seguimiento.TipoServicio,
	// 	seguimiento.Servicio,
	// 	seguimiento.Otro)
	// CREAR RECOMENDACION
	// k := models.CrearRecomendacion(
	// 	recomendacion.EstadoRecomendacion,
	// 	recomendacion.EstadoPastillaA,
	// 	recomendacion.EstadoPastillaB,
	// 	recomendacion.EstadoPastillaC,
	// 	recomendacion.EstadoPastillaD,
	// 	recomendacion.RepuestoGeneral,
	// 	recomendacion.RepuestoEspecifico,
	// 	recomendacion.RepuestoAlternativo,
	// 	recomendacion.TiempoReparacion,
	// 	recomendacion.BreveRecomendacion,
	// 	recomendacion.Presupuesto,
	// 	recomendacion.HoraInicioRecepcion,
	// 	recomendacion.HoraFinRecepcion,
	// 	recomendacion.HoraInicioLavado,
	// 	recomendacion.HoraFinLavado,
	// 	recomendacion.HoraInicioMantto,
	// 	recomendacion.HoraFinMantto,
	// 	recomendacion.HoraInicioEntrega,
	// 	recomendacion.HoraFinEntrega,
	// 	recomendacion.FechaRegistro,
	// 	recomendacion.HoraRegistro,
	// 	recomendacion.IdPresupuesto)

	// CREAR CONTACTO
	a := models.CrearContacto(contacto.TrabajadorCorasur,
		contacto.TipoContactoCorasur,
		contacto.ContactoCorasur,
		contacto.Placa,
		contacto.IdVehiculo,
		contacto.Cliente,
		contacto.TipoContactoCliente,
		contacto.ContactoCliente,
		contacto.Activo,
		contacto.ResultadoContacto,
		contacto.FechaRetomarContacto,
		contacto.UltimoKilometrajeServicio,
		timestamp.FromNow{}.String(),
		contacto.IdMotivo,
		contacto.IdEstado,
		contacto.IdRespuesta,
		contacto.ManttoK,
		contacto.NuevoDuenio,
		contacto.TelefonoNuevoDuenio,
		contacto.AgendaCita,
		contacto.EstadoContacto,
		contacto.VozCliente,
		contacto.FechaCitaReferencial1,
		contacto.DocumentoRecojo,
		contacto.FechaRecojoDocumento,
		contacto.Otro,
		d.IdNoCita,
		0, // e.IdPresupuesto,
		0, // f.IdOrdenTrabajo,
		0, // g.IdPostergacion,
		0, // h.IdEncuesta,
		0, // i.IdSeguimiento,
		0,
		contacto.MotivoDetalle,
		contacto.MotivoDetalleDescripcion, // k.IdRecomendacion
	)

	// variables de contacto reutilizadas en pronostrico
	fechaLlamadaEfectuada := contacto.FechaRegistro
	contesta := contacto.Activo
	resultado2 := contacto.ResultadoContacto
	fechaPactadaVolverLlamar := contacto.FechaRetomarContacto

	// CREAR CITA
	// fmt.Println(cita)
	for i := range cita {
		// fmt.Println("##########################################")
		citai := cita[i]
		b := models.CrearCita(
			citai.Servicio,
			citai.DetalleServicio,
			citai.FechaCita,
			citai.FechaPactadaCita,
			citai.MotivoCita,
			citai.Detallecita,
			citai.ObservacionCita,
			citai.AclaracionRecordatorio,
			citai.FechaCitaCambiada,
			citai.HoraCitaCambiada,
			citai.PersonaCambioCita,
			citai.RazonCambioCita,
			citai.ObservacionCambio,
			citai.MensajeContactoCita,
			citai.FechaNuevoContacto,
			citai.PersonaCancelaCita,
			citai.MotivoCancelaCita,
			citai.FechaRetomarContactoCita,
			citai.VozCliente,
			citai.HoraPosibleArribo,
			citai.EstadoReprogramar,
			citai.HoraInicio,
			citai.HoraEntrega,
			citai.FechaCitaReprogramada,
			citai.ObservacionReprogramacion,
			citai.DetalleIncumplimiento,
			citai.EstadoCita,
			citai.IdServicio,
			a.IdContacto,
			citai.IdMotivoResultado)
		fmt.Println(b)

		// fmt.Println(citai)
		// fmt.Println("##########################################")
	}

	// variables usadas de cita en el pronostico PRUEBA

	// fechaCita := cita[0].FechaCita
	fmt.Println("====================ID PROSNOSTICOASCA==========================")
	fmt.Println(pronostico5k.IdPronostico5k)
	fmt.Println("==============================================")
	if pronostico5k.IdPronostico5k > 0 {
		// id int64,fechallamada,contesta,resultado2,fechapvl,fechac string
		models.ActualizarPro(
			/*
				pronostico5k.IdPronostico5k,
				pronostico5k.FechaLlamadaEfectuada,
				pronostico5k.Contesta,
				pronostico5k.Resultado2,
				pronostico5k.FechaPactadaVolverLlamar,
				pronostico5k.FechaCita,
			*/
			pronostico5k.IdPronostico5k,
			fechaLlamadaEfectuada,
			contesta,
			resultado2,
			fechaPactadaVolverLlamar,
			pronostico5k.FechaCita,
		)

	} else {
		ordenes := models.OrdenesPronostico(contacto.Placa)

		ultimaOrden := ordenes[0]
		penultimaOrden := ordenes[1]

		//ejemplo
		km := ultimaOrden.Km - penultimaOrden.Km

		kmpenultserv := ultimaOrden.Km
		kmultimoserv := penultimaOrden.Km

		ultimomanto := ultimaOrden.Servicio

		fmt.Println(km)

		// numero de días
		f1, _ := time.Parse(time.RFC3339, penultimaOrden.FechaInicio)
		f2, _ := time.Parse(time.RFC3339, ultimaOrden.FechaInicio)
		diff := f2.Sub(f1)
		numeroDias := int64(diff.Hours() / 24)

		// // kilometraje que lo toca
		mattoQueLeToca := math.Trunc((float64(ultimaOrden.Km)+5000)/5000) * 5000

		promedioRecorridoHastaelDia := float64(km) / float64(numeroDias)

		loqueFaltaparaManto := int64(mattoQueLeToca - float64(kmpenultserv))

		cantDiasparamanto := float64(loqueFaltaparaManto) / promedioRecorridoHastaelDia

		//pronostic := f2.AddDate(0, 0, int(cantDiasparamanto))
		fechaError := (f2.AddDate(0, 0, int(cantDiasparamanto)).String())
		pronostic := fechaError[0:19]

		// fecha 15 antes del pronostico

		fechaParacalculo := f2.AddDate(0, 0, int(cantDiasparamanto))
		fecha15anpron := (fechaParacalculo.AddDate(0, 0, -15)).String()
		fecha15anpronUlt := fecha15anpron[0:19]
		fmt.Println(pronostic)
		fmt.Println("***************************")
		fmt.Println(fecha15anpron)
		fmt.Println(fecha15anpronUlt)
		l := models.CrearPronostico5k(
			a.Placa,
			c.Modelo,
			a.Cliente,
			a.ContactoCliente,
			a.ContactoCliente,
			ultimomanto,
			pronostico5k.Prioridad,
			// new
			kmultimoserv,
			kmpenultserv,
			km,
			mattoQueLeToca,
			penultimaOrden.FechaInicio,
			ultimaOrden.FechaInicio,
			numeroDias,
			promedioRecorridoHastaelDia,
			loqueFaltaparaManto,
			cantDiasparamanto,
			// campo pronostico
			pronostic,
			// det
			fecha15anpronUlt,
			pronostico5k.FechaPactadaUltimoContacto,
			pronostico5k.FechaCorregida,
			pronostico5k.FechaCorregidaSistema,
			pronostico5k.FechaWppEnvio,
			pronostico5k.Vistobueno,
			pronostico5k.Resultado,
			fechaLlamadaEfectuada,
			contesta,
			resultado2,
			fechaPactadaVolverLlamar,
			pronostico5k.FechaCita,
			pronostico5k.Recordatorio,
			timestamp.FromNow{}.String(),
			pronostico5k.NroPresupuesto,
			pronostico5k.Recomendacion,
			pronostico5k.Costo)
		fmt.Println(l)

	}

	fmt.Println(c)

	models.Closeconnection()
	m := models.Message{Code: 201, Message: "Registro Exitoso!"}
	j, err := json.Marshal(m)
	if err != nil {
		log.Fatalf("Error al convertir el mensaje: %s", err)
	}
	w.WriteHeader(http.StatusCreated)
	w.Write(j)

}
