package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
	"github.com/gorilla/mux"
)

// SelectContacto controlador de prueba 3
func InsertarVehiculoEntrega(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vehiculoentrega := models.VehiculoEntrega{}
	err := json.NewDecoder(r.Body).Decode(&vehiculoentrega)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}

	b := models.CrearVehiculoEntrega(
		vehiculoentrega.FechaProgramacionEntrega,
		vehiculoentrega.FechaEntregaVehiculo,
		vehiculoentrega.PersonaEntrega,
		vehiculoentrega.Telefono,
		vehiculoentrega.Celular,
		vehiculoentrega.OpinionEntrega,
		vehiculoentrega.FechaCitaReferencial2,
		vehiculoentrega.IdVehiculo,
		vehiculoentrega.IdVehiculoUso,
		vehiculoentrega.IdDepartamento,
		vehiculoentrega.IdProvincia)
	fmt.Println(b)
	models.Closeconnection()
}

func ListarVehiculosEntrega(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vehiculol := models.ListarVehiculosEntrega()
	j, err := json.Marshal(vehiculol)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarVehiculoEntrega(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	serie := vars["serie"] //Esto ya es un String
	res := models.ListarVehiculo(serie)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
