package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
	"github.com/gorilla/mux"
)

// SelectContacto controlador de prueba 3
func InsertarNoCita(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	nocita := models.NoCita{}
	err := json.NewDecoder(r.Body).Decode(&nocita)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	a := models.CrearNoCita(nocita.ClaseRazon,
		nocita.DescripcionNoCita,
		nocita.SabeKm,
		nocita.KmFaltaRecorrido,
		nocita.SabePromedio,
		nocita.PromedioRecorrido,
		nocita.UsoVehicular,
		nocita.FechaPropuesta,
		nocita.FechaSugeridaCliente,
		nocita.FechaCorregida,
		nocita.FechaFinal,
		nocita.FechaSiguienteLlamada,
		nocita.Concesionario,
		nocita.CausaRechazoCita,
		nocita.CausaRechazoCorasur,
		nocita.ObservacionesNoCita,
		nocita.Comentarios,
		nocita.IdRazonNoCita)
	fmt.Println(a)
	models.Closeconnection()
}

func ListarNoCitas(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	nocital := models.ListarNoCitas()
	j, err := json.Marshal(nocital)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ActualizarNoCita(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	nocita := models.NoCita{}
	err := json.NewDecoder(r.Body).Decode(&nocita)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	a := models.CrearNoCita(nocita.ClaseRazon,
		nocita.DescripcionNoCita,
		nocita.SabeKm,
		nocita.KmFaltaRecorrido,
		nocita.SabePromedio,
		nocita.PromedioRecorrido,
		nocita.UsoVehicular,
		nocita.FechaPropuesta,
		nocita.FechaSugeridaCliente,
		nocita.FechaCorregida,
		nocita.FechaFinal,
		nocita.FechaSiguienteLlamada,
		nocita.Concesionario,
		nocita.CausaRechazoCita,
		nocita.CausaRechazoCorasur,
		nocita.ObservacionesNoCita,
		nocita.Comentarios,
		nocita.IdRazonNoCita)
	fmt.Println(a)
	models.Closeconnection()
}

func EliminarRazon(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	defer r.Body.Close()
	nocita := models.NoCita{}
	if err := json.NewDecoder(r.Body).Decode(&nocita); err != nil {
		nocita.Delete()
	}
	models.Closeconnection()
}

func ListarNoCitasParametro(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	claseRazon := vars["claseRazon"] //Esto ya es un String
	res := models.ListarNoCitasParametro(claseRazon)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()

}
