package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../commons"
	"../configuration"
	"../models"
	"github.com/gorilla/mux"
)

// SelectContacto controlador de prueba 3
func InsertarVehiculo(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vehiculo := models.Vehiculo{}
	err := json.NewDecoder(r.Body).Decode(&vehiculo)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	// conexion
	db, err := configuration.OpenDB()
	checkErr(err)
	defer db.Close()
	/****************************VERIFICAR QUE NO EXISTA EL USUARIO****************************/
	// setear consulta
	existe, err := db.Prepare(`SELECT serie FROM x_tavehiculo WHERE serie = ?`)
	checkErr(err)
	defer existe.Close()

	rows, err := existe.Query(vehiculo.Serie)
	checkErr(err)

	if rows.Next() {
		m := models.Message{
			Message: "El  ya existe",
			Code:    http.StatusBadRequest,
		}
		commons.DisplayMessage(w, m)
		return
	}
	/********************************************************************/

	a := models.CrearVehiculo(vehiculo.Serie,
		vehiculo.Placa,
		vehiculo.Motor,
		vehiculo.Chasis,
		vehiculo.Vin,
		vehiculo.AnioFabricacion,
		vehiculo.Modelo)
	fmt.Println(a)
	models.Closeconnection()
}

func ListarVehiculos(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vehiculol := models.ListarVehiculos()
	j, err := json.Marshal(vehiculol)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarVehiculo(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	serie := vars["serie"] //Esto ya es un String
	res := models.ListarVehiculo(serie)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
