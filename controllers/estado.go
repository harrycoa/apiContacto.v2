package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"../models"
	"github.com/gorilla/mux"
)

// SelectEstado controlador de prueba 3
func ListarEstados(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	fmt.Println(r.URL.Path)
	s := r.URL.Path
	primera := s[0:1]
	ultima := s[len(s)-1:]
	fmt.Println(primera)
	fmt.Println(ultima)
	estado := models.ListarEstados()
	j, err := json.Marshal(estado)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
func ListarEstadoParametro(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	eclase := vars["eclase"] //Esto ya es un String
	//fmt.Fprintf(w, eclase)
	//fmt.Println(eclase)
	res := models.ListarEstadosParametro(eclase)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
