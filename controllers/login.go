package controllers

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"../commons"
	"../configuration"
	"../models"
	"../models/auth"
	"../models/user"
	"../results"
)

// Login es el controlador de login
func Login(w http.ResponseWriter, r *http.Request) {

	credenciales := user.Credentials{}

	err := json.NewDecoder(r.Body).Decode(&credenciales)

	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}

	db, err := configuration.OpenDB()
	checkErr(err)
	defer db.Close()

	// setear consulta
	querySQL := `SELECT nombreUsuario
					, claveUsuario
					, estado
					, dni
					, nombreCompleto
					, email
					, telefono
			  FROM x_taUsuario WHERE nombreUsuario = ?`
	selectUser, err := db.Prepare(querySQL)
	defer selectUser.Close()
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	rows, err := selectUser.Query(credenciales.NombreUsuario)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	if !rows.Next() {
		res := auth.Response{Key: "", Mensaje: "Usuario no válido"}
		j, err := json.Marshal(res)
		if err != nil {
			log.Fatal(err)
		}
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}

	u := user.TokenUser{}
	pwdUser := ""

	err = rows.Scan(&u.NombreUsuario, &pwdUser, &u.Estado, &u.DNI, &u.NombreCompleto, &u.Email, &u.Telefono)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	c := sha256.Sum256([]byte(credenciales.ClaveUsuario))
	pwd := fmt.Sprintf("%x", c)

	fmt.Println(pwd)
	fmt.Println(pwdUser)

	res := auth.Response{Key: "", Mensaje: "Contraseña incorrecta"}

	if pwd == pwdUser {

		// seleccionar los roles del usuario
		querySelectRol := `SELECT idRolUsuario FROM x_taRolUsuario WHERE nombreUsuario = ?`
		selectRoles, err := db.Prepare(querySelectRol)
		defer selectRoles.Close()
		if err != nil {
			results.ErrorResult(w, err.Error())
			return
		}

		rows, err := selectRoles.Query(credenciales.NombreUsuario)
		if err != nil {
			results.ErrorResult(w, err.Error())
			return
		}

		for rows.Next() {
			var rol int
			if err = rows.Scan(&rol); err != nil {
				log.Fatal(err)
			}
			u.Rol = append(u.Rol, rol)
		}

		res.Key = commons.GenerateJWT(u)
		res.Mensaje = "Usuario válido"
	}

	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(j)
}
