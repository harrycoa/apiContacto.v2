package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"../models"
	"github.com/gorilla/mux"
)

// SelectRespuesta controlador de prueba 3
func ListarRespuestas(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	res := models.ListarRespuestas()
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()

}
func ListarRespuesta(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	respuesta := vars["respuesta"] //Esto ya es un String
	fmt.Fprintf(w, respuesta)
	fmt.Println(respuesta)
	res := models.ListarRespuesta(respuesta)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
func InsertarRespuesta(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	respuesta := models.Respuesta{}
	err := json.NewDecoder(r.Body).Decode(&respuesta)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	a := models.CrearRespuesta(respuesta.Rclase, respuesta.Respuesta)
	fmt.Println(a)
	models.Closeconnection()
}

func ListarRespuestaParametro(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	rclase := vars["rclase"] //Esto ya es un String
	//fmt.Fprintf(w, mclase)
	//fmt.Println(mclase)
	res := models.ListarRespuestasParametro(rclase)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
