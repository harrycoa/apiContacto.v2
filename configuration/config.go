package configuration

import (
	"fmt"

	"github.com/ziutek/mymysql/mysql"
	_ "github.com/ziutek/mymysql/native" // Native engine
)

func ExecSp(spStr string, params ...interface{}) (mysql.Result, error) {

	c := getConfiguration()
	server := fmt.Sprintf("%s:%s", c.Server, c.Port)
	db := mysql.New("tcp", "", server, c.User, c.Password, c.Database)

	err := db.Connect()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	sp, err := db.Prepare(spStr)
	if err != nil {
		return nil, err
	}

	res, err := sp.Run(params...)
	if err != nil {
		return nil, err
	}

	return res, err
}

// ExecQuerySp ...
func ExecQuerySp(procedimiento string, params ...interface{}) ([]mysql.Row, error) {

	c := getConfiguration()
	server := fmt.Sprintf("%s:%s", c.Server, c.Port)
	db := mysql.New("tcp", "", server, c.User, c.Password, c.Database)

	err := db.Connect()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	sp, err := db.Prepare(procedimiento)
	if err != nil {
		return nil, err
	}

	rows, _, err := sp.Exec(params...)

	return rows, err
}

// ExecQueryRowSp ...
func ExecQueryRowSp(procedimiento string, params ...interface{}) (mysql.Row, error) {

	c := getConfiguration()
	server := fmt.Sprintf("%s:%s", c.Server, c.Port)
	db := mysql.New("tcp", "", server, c.User, c.Password, c.Database)

	err := db.Connect()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	sp, err := db.Prepare(procedimiento)
	if err != nil {
		return nil, err
	}

	row, _, err := sp.ExecFirst(params...)

	return row, err
}
