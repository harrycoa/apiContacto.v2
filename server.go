package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"./commons"
	"./routes"

	"github.com/rs/cors"
	"github.com/urfave/negroni"
)

func main() {

	flag.IntVar(&commons.Port, "port", 9000, "Puerto en el que escuchará el servidor")
	flag.Parse()

	// cors
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		// AllowedOrigins:   []string{"http://localhost:4200"},
		AllowCredentials: true,
		// Enable Debugging for testing, consider disabling in production
		Debug: true,
	})

	// inicia las rutas
	router := routes.InitRoutes()

	//inicia los middlewares
	n := negroni.Classic()

	//cors
	n.Use(c)

	n.UseHandler(router)

	server := &http.Server{Addr: fmt.Sprintf(":%d", commons.Port), Handler: n}

	log.Printf("Servidor escuchando en http://localhost:%d", commons.Port)
	log.Println(server.ListenAndServe())
	log.Println("Finalizó la ejecución del servidor")
}
