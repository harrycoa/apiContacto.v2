package models

import "time"

type Postergacion struct {
	IdPostergacion           int64  `json:"idPostergacion"`
	FechaEntregaReprogramada string `json:"fechaEntregaReprogramada"`
	MotivoAceptacion         string `json:"motivoAceptacion"`
	VozCliente               string `json:"vozCliente"`
	MotivoCambioFecha        string `json:"motivoCambioFecha"`
	IdMotivoResultado        int64
}

const postergacionEsquema string = `CREATE TABLE x_tapostergacion (
	idPostergacion int(11) NOT NULL AUTO_INCREMENT,
	fechaEntregaReprogramada datetime DEFAULT NULL,
	motivoAceptacion varchar(200) DEFAULT NULL,
	vozCliente varchar(200) DEFAULT NULL,
	motivoCambioFecha varchar(200) DEFAULT NULL,
	idMotivoResultado int(11) DEFAULT NULL,
	PRIMARY KEY (idPostergacion)
  );`

type Postergaciones []Postergacion

func ListarPostergaciones() Postergaciones {
	sql := `SELECT idPostergacion, 
					fechaEntregaReprogramada, 
					motivoAceptacion, 
					vozCliente, 
					motivoCambioFecha,
				    idMotivoResultado 
					FROM x_taordentrabajo`
	listado := Postergaciones{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Postergacion{}
		rows.Scan(&obj.IdPostergacion,
			&obj.FechaEntregaReprogramada,
			&obj.MotivoAceptacion,
			&obj.VozCliente,
			&obj.MotivoCambioFecha,
			&obj.IdMotivoResultado)
		listado = append(listado, obj)
	}
	return listado
}

func ListarPostergacion(idPostergacion string) Postergaciones {
	sql := `SELECT idPostergacion, 
					fechaEntregaReprogramada, 
					motivoAceptacion, 
					vozCliente, 
					motivoCambioFecha,
				    idMotivoResultado 
					FROM x_taordentrabajo where idPostergacion=?`
	listado := Postergaciones{}
	rows, _ := Query(sql, idPostergacion)
	for rows.Next() {
		obj := Postergacion{}
		rows.Scan(&obj.IdPostergacion,
			&obj.FechaEntregaReprogramada,
			&obj.MotivoAceptacion,
			&obj.VozCliente,
			&obj.MotivoCambioFecha,
			&obj.IdMotivoResultado)
		listado = append(listado, obj)
	}
	return listado
}

func NuevaPostergacion(
	fechaEntregaReprogramada,
	motivoAceptacion,
	vozCliente,
	motivoCambioFecha string,
	idMotivoResultado int64) *Postergacion {
	obj := &Postergacion{
		FechaEntregaReprogramada: fechaEntregaReprogramada,
		MotivoAceptacion:         motivoAceptacion,
		VozCliente:               vozCliente,
		MotivoCambioFecha:        motivoCambioFecha,
		IdMotivoResultado:        idMotivoResultado,
	}
	return obj
}

func CrearPostergacion(
	fechaEntregaReprogramada,
	motivoAceptacion,
	vozCliente,
	motivoCambioFecha string,
	idMotivoResultado int64) *Postergacion {
	obj := NuevaPostergacion(
		fechaEntregaReprogramada,
		motivoAceptacion,
		vozCliente,
		motivoCambioFecha,
		idMotivoResultado)
	obj.Guardar()
	return obj
}

func (this *Postergacion) Guardar() {
	if this.IdPostergacion == 0 {
		this.insertar()
	}
}

func (this *Postergacion) insertar() {
	sql := `INSERT x_tapostergacion SET fechaEntregaReprogramada=?, 
											motivoAceptacion=?, 
											vozCliente=?, 									
											motivoCambioFecha=?, 
										    idMotivoResultado=?`
	fechaER, _ := time.Parse(time.RFC3339, this.FechaEntregaReprogramada)
	result, err := Exec(sql,
		fechaER,
		this.MotivoAceptacion,
		this.VozCliente,
		this.MotivoCambioFecha,
		this.IdMotivoResultado)
	this.IdPostergacion, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
	} else {
		println("LastInsertId:", this.IdPostergacion)
	}
}
