package models

import "time"

type Recomendacion struct {
	IdRecomendacion     int64  `json:"idRecomendacion"`
	EstadoRecomendacion string `json:"estadoRecomendacion"`
	EstadoPastillaA     string `json:"estadoPastillaA"`
	EstadoPastillaB     string `json:"estadoPastillaB"`
	EstadoPastillaC     string `json:"estadoPastillaC"`
	EstadoPastillaD     string `json:"estadoPastillaD"`
	RepuestoGeneral     string `json:"repuestoGeneral"`
	RepuestoEspecifico  string `json:"repuestoEspecifico"`
	RepuestoAlternativo string `json:"repuestoAlternativo"`
	TiempoReparacion    string `json:"tiempoReparacion"`
	BreveRecomendacion  string `json:"breveRecomendacion"`
	Presupuesto         string `json:"presupuesto"`
	HoraInicioRecepcion string `json:"horaInicioRecepcion"`
	HoraFinRecepcion    string `json:"horaFinRecepcion"`
	HoraInicioLavado    string `json:"horaInicioLavado"`
	HoraFinLavado       string `json:"horaFinLavado"`
	HoraInicioMantto    string `json:"horaInicioMantto"`
	HoraFinMantto       string `json:"horaFinMantto"`
	HoraInicioEntrega   string `json:"horaInicioEntrega"`
	HoraFinEntrega      string `json:"horaFinEntrega"`
	FechaRegistro       string `json:"fechaRegistro"`
	HoraRegistro        string `json:"horaRegistro"`
	IdPresupuesto       int64
}

const recomendacionEsquema string = `CREATE TABLE x_tarecomendacion (
	idRecomendacion int(11) NOT NULL AUTO_INCREMENT,
	estadoRecomendacion varchar(100) NULL, 
	estadoPastillaA varchar(100) NULL,  
	estadoPastillaB varchar(100) NULL,  
	estadoPastillaC varchar(100) NULL,  
	estadoPastillaD varchar(100) NULL,  
	repuestoGeneral varchar(100) NULL, 
	repuestoEspecifico varchar(100) NULL, 
	repuestoAlternativo varchar(100) NULL, 
	tiempoReparacion varchar(100) NULL, 
	breveRecomendacion varchar(100) NULL,  
	presupuesto char(1),
	horaInicioRecepcion time NULL,
	horaFinRecepcion time NULL,
	horaInicioLavado time NULL,
	horaFinLavado time NULL,
	horaInicioMantto time NULL,
	horaFinMantto time NULL,
	horaInicioEntrega time NULL,
	horaFinEntrega time NULL,  
	fechaRegistro datetime,
	horaRegistro time NULL,
	idPresupuesto int(11),
	PRIMARY KEY (idRecomendacion)
  );`

type Recomendaciones []Recomendacion

func ListarRecomendaciones() Recomendaciones {
	sql := `SELECT idRecomendacion,
					estadoRecomendacion,
					estadoPastillaA,
					estadoPastillaB,
					estadoPastillaC,
					estadoPastillaD,
					repuestoGeneral,
					repuestoEspecifico,
					repuestoAlternativo,
					tiempoReparacion,
					breveRecomendacion,
					presupuesto,
					horaInicioRecepcion,
					horaFinRecepcion,
					horaInicioLavado,
					horaFinLavado,
					horaInicioMantto,
					horaFinMantto,
					horaInicioEntrega,
					horaFinEntrega,
					fechaRegistro,
					horaRegistro,
					idPresupuesto
				FROM x_tarecomendacion`
	listado := Recomendaciones{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Recomendacion{}
		rows.Scan(&obj.IdRecomendacion,
			&obj.EstadoRecomendacion,
			&obj.EstadoPastillaA,
			&obj.EstadoPastillaB,
			&obj.EstadoPastillaC,
			&obj.EstadoPastillaD,
			&obj.RepuestoGeneral,
			&obj.RepuestoEspecifico,
			&obj.RepuestoAlternativo,
			&obj.TiempoReparacion,
			&obj.BreveRecomendacion,
			&obj.Presupuesto,
			&obj.HoraInicioRecepcion,
			&obj.HoraFinRecepcion,
			&obj.HoraInicioLavado,
			&obj.HoraFinLavado,
			&obj.HoraInicioMantto,
			&obj.HoraFinMantto,
			&obj.HoraInicioEntrega,
			&obj.HoraFinEntrega,
			&obj.FechaRegistro,
			&obj.HoraRegistro,
			&obj.IdPresupuesto)
		listado = append(listado, obj)
	}
	return listado
}

func ListarRecomendacion(idRecomendacion string) Recomendaciones {
	sql := `SELECT idRecomendacion,
					estadoRecomendacion,
					estadoPastillaA,
					estadoPastillaB,
					estadoPastillaC,
					estadoPastillaD,
					repuestoGeneral,
					repuestoEspecifico,
					repuestoAlternativo,
					tiempoReparacion,
					breveRecomendacion,
					presupuesto,
					horaInicioRecepcion,
					horaFinRecepcion,
					horaInicioLavado,
					horaFinLavado,
					horaInicioMantto,
					horaFinMantto,
					horaInicioEntrega,
					horaFinEntrega,
					fechaRegistro,
					horaRegistro,
					idPresupuesto
				FROM x_tarecomendacion`
	listado := Recomendaciones{}
	rows, _ := Query(sql, idRecomendacion)
	for rows.Next() {
		obj := Recomendacion{}
		rows.Scan(&obj.IdRecomendacion,
			&obj.EstadoRecomendacion,
			&obj.EstadoPastillaA,
			&obj.EstadoPastillaB,
			&obj.EstadoPastillaC,
			&obj.EstadoPastillaD,
			&obj.RepuestoGeneral,
			&obj.RepuestoEspecifico,
			&obj.RepuestoAlternativo,
			&obj.TiempoReparacion,
			&obj.BreveRecomendacion,
			&obj.Presupuesto,
			&obj.HoraInicioRecepcion,
			&obj.HoraFinRecepcion,
			&obj.HoraInicioLavado,
			&obj.HoraFinLavado,
			&obj.HoraInicioMantto,
			&obj.HoraFinMantto,
			&obj.HoraInicioEntrega,
			&obj.HoraFinEntrega,
			&obj.FechaRegistro,
			&obj.HoraRegistro,
			&obj.IdPresupuesto)
		listado = append(listado, obj)
	}
	return listado
}

func NuevaRecomendacion(
	estadoRecomendacion,
	estadoPastillaA,
	estadoPastillaB,
	estadoPastillaC,
	estadoPastillaD,
	repuestoGeneral,
	repuestoEspecifico,
	repuestoAlternativo,
	tiempoReparacion,
	breveRecomendacion,
	presupuesto,
	horaInicioRecepcion,
	horaFinRecepcion,
	horaInicioLavado,
	horaFinLavado,
	horaInicioMantto,
	horaFinMantto,
	horaInicioEntrega,
	horaFinEntrega,
	fechaRegistro,
	horaRegistro string,
	idPresupuesto int64) *Recomendacion {
	obj := &Recomendacion{
		EstadoRecomendacion: estadoRecomendacion,
		EstadoPastillaA:     estadoPastillaA,
		EstadoPastillaB:     estadoPastillaB,
		EstadoPastillaC:     estadoPastillaC,
		EstadoPastillaD:     estadoPastillaD,
		RepuestoGeneral:     repuestoGeneral,
		RepuestoEspecifico:  repuestoEspecifico,
		RepuestoAlternativo: repuestoAlternativo,
		TiempoReparacion:    tiempoReparacion,
		BreveRecomendacion:  breveRecomendacion,
		Presupuesto:         presupuesto,
		HoraInicioRecepcion: horaInicioRecepcion,
		HoraFinRecepcion:    horaFinRecepcion,
		HoraInicioLavado:    horaInicioLavado,
		HoraFinLavado:       horaFinLavado,
		HoraInicioMantto:    horaInicioMantto,
		HoraFinMantto:       horaFinMantto,
		HoraInicioEntrega:   horaInicioEntrega,
		HoraFinEntrega:      horaFinEntrega,
		FechaRegistro:       fechaRegistro,
		HoraRegistro:        horaRegistro,
		IdPresupuesto:       idPresupuesto,
	}
	return obj
}

func CrearRecomendacion(
	estadoRecomendacion,
	estadoPastillaA,
	estadoPastillaB,
	estadoPastillaC,
	estadoPastillaD,
	repuestoGeneral,
	repuestoEspecifico,
	repuestoAlternativo,
	tiempoReparacion,
	breveRecomendacion,
	presupuesto,
	horaInicioRecepcion,
	horaFinRecepcion,
	horaInicioLavado,
	horaFinLavado,
	horaInicioMantto,
	horaFinMantto,
	horaInicioEntrega,
	horaFinEntrega,
	fechaRegistro,
	horaRegistro string,
	idPresupuesto int64) *Recomendacion {
	obj := NuevaRecomendacion(
		estadoRecomendacion,
		estadoPastillaA,
		estadoPastillaB,
		estadoPastillaC,
		estadoPastillaD,
		repuestoGeneral,
		repuestoEspecifico,
		repuestoAlternativo,
		tiempoReparacion,
		breveRecomendacion,
		presupuesto,
		horaInicioRecepcion,
		horaFinRecepcion,
		horaInicioLavado,
		horaFinLavado,
		horaInicioMantto,
		horaFinMantto,
		horaInicioEntrega,
		horaFinEntrega,
		fechaRegistro,
		horaRegistro,
		idPresupuesto)
	obj.Guardar()
	return obj
}

func (this *Recomendacion) Guardar() {
	if this.IdRecomendacion == 0 {
		this.insertar()
	}
}

func (this *Recomendacion) insertar() {
	sql := `INSERT x_tarecomendacion SET estadoRecomendacion=?,
										estadoPastillaA=?,
										estadoPastillaB=?,
										estadoPastillaC=?,
										estadoPastillaD=?,
										repuestoGeneral=?,
										repuestoEspecifico=?,
										repuestoAlternativo=?,
										tiempoReparacion=?,
										breveRecomendacion=?,
										presupuesto=?,
										horaInicioRecepcion=?,
										horaFinRecepcion=?,
										horaInicioLavado=?,
										horaFinLavado=?,
										horaInicioMantto=?,
										horaFinMantto=?,
										horaInicioEntrega=?,
										horaFinEntrega=?,
										fechaRegistro=?,
										horaRegistro=?,
										idPresupuesto=?`

	HorainiR, _ := time.Parse(time.RFC3339, this.HoraInicioRecepcion)
	HorafinR, _ := time.Parse(time.RFC3339, this.HoraFinRecepcion)
	HorainiL, _ := time.Parse(time.RFC3339, this.HoraInicioLavado)
	HorafinL, _ := time.Parse(time.RFC3339, this.HoraFinLavado)
	HorainiM, _ := time.Parse(time.RFC3339, this.HoraInicioMantto)
	HorafinM, _ := time.Parse(time.RFC3339, this.HoraFinMantto)
	HorainiE, _ := time.Parse(time.RFC3339, this.HoraInicioEntrega)
	HorafinE, _ := time.Parse(time.RFC3339, this.HoraFinEntrega)
	FechaReg, _ := time.Parse(time.RFC3339, this.FechaRegistro)
	HoraReg, _ := time.Parse(time.RFC3339, this.HoraRegistro)
	result, err := Exec(sql,
		this.EstadoRecomendacion,
		this.EstadoPastillaA,
		this.EstadoPastillaB,
		this.EstadoPastillaC,
		this.EstadoPastillaD,
		this.RepuestoGeneral,
		this.RepuestoEspecifico,
		this.RepuestoAlternativo,
		this.TiempoReparacion,
		this.BreveRecomendacion,
		this.Presupuesto,
		HorainiR,
		HorafinR,
		HorainiL,
		HorafinL,
		HorainiM,
		HorafinM,
		HorainiE,
		HorafinE,
		FechaReg,
		HoraReg,
		this.IdPresupuesto)
	this.IdRecomendacion, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
	} else {
		println("LastInsertId:", this.IdRecomendacion)
	}
}
