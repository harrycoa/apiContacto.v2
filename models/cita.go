package models

import (
	"time"
)

type Cita struct {
	IdCita                    int64  `json:"idCita"`
	Servicio                  string `json:"servicio"`
	DetalleServicio           string `json:"detalleServicio"`
	FechaCita                 string `json:"fechaCita"`
	FechaPactadaCita          string `json:"fechaPactadaCita"`
	MotivoCita                string `json:"motivoCita"`
	Detallecita               string `json:"detallecita"`
	ObservacionCita           string `json:"observacionCita"`
	AclaracionRecordatorio    string `json:"aclaracionRecordatorio"`
	FechaCitaCambiada         string `json:"fechaCitaCambiada"`
	HoraCitaCambiada          string `json:"horaCitaCambiada"`
	PersonaCambioCita         string `json:"personaCambioCita"`
	RazonCambioCita           string `json:"razonCambioCita"`
	ObservacionCambio         string `json:"observacionCambio"`
	MensajeContactoCita       string `json:"mensajeContactoCita"`
	FechaNuevoContacto        string `json:"fechaNuevoContacto"`
	PersonaCancelaCita        string `json:"personaCancelaCita"`
	MotivoCancelaCita         string `json:"motivoCancelaCita"`
	FechaRetomarContactoCita  string `json:"fechaRetomarContactoCita"`
	VozCliente                string `json:"vozCliente"`
	HoraPosibleArribo         string `json:"horaPosibleArribo"`
	EstadoReprogramar         string `json:"estadoReprogramar"`
	HoraInicio                string `json:"horaInicio"`
	HoraEntrega               string `json:"horaEntrega"`
	FechaCitaReprogramada     string `json:"fechaCitaReprogramada"`
	ObservacionReprogramacion string `json:"observacionReprogramacion"`
	DetalleIncumplimiento     string `json:"detalleIncumplimiento"`
	EstadoCita                string `json:"estadoCita"`
	IdMotivoResultado         int64
	IdServicio                int64
	IdContacto                int64
}

type Citas []Cita

const citaEsquema string = `CREATE TABLE x_tacita (
	idCita int(11) NOT NULL AUTO_INCREMENT,
	servicio varchar(100) NULL,
	detalleServicio varchar(200) NULL,
	fechaCita datetime NOT NULL,
	fechaPactadaCita datetime DEFAULT NULL,
	motivoCita varchar(100) DEFAULT NULL,
	detallecita varchar(100) DEFAULT NULL,
	observacionCita varchar(100) DEFAULT NULL,
	aclaracionRecordatorio varchar(100) DEFAULT NULL,
	fechaCitaCambiada datetime DEFAULT NULL,
	horaCitaCambiada datetime DEFAULT NULL,
	personaCambioCita varchar(200) DEFAULT NULL,
	razonCambioCita varchar(100) DEFAULT NULL,
	observacionCambio varchar(200) DEFAULT NULL,
	mensajeContactoCita varchar(200) DEFAULT NULL,
	fechaNuevoContacto datetime DEFAULT NULL,
	personaCancelaCita varchar(100) DEFAULT NULL,
	motivoCancelaCita varchar(100) DEFAULT NULL,
	fechaRetomarContactoCita datetime DEFAULT NULL,
	vozCliente varchar(200) DEFAULT NULL,
	horaPosibleArribo time DEFAULT NULL,
	estadoReprogramar char(1) DEFAULT NULL,
	horaInicio time DEFAULT NULL,
	horaEntrega time DEFAULT NULL,
	fechaCitaReprogramada time DEFAULT NULL,
	observacionReprogramacion varchar(100) DEFAULT NULL,
	detalleIncumplimiento varchar(100) DEFAULT NULL,
	estadoCita char(1) DEFAULT NULL,
	idServicio int(11) DEFAULT NULL,
	idContacto int(11) DEFAULT NULL,
	idMotivoResultado int(11) DEFAULT NULL,
	PRIMARY KEY (idCita)
  );`

// Constructores
func NuevaCita(servicio,
	detalleServicio,
	fechaCita,
	fechaPactadaCita,
	motivoCita,
	detallecita,
	observacionCita,
	aclaracionRecordatorio,
	fechaCitaCambiada,
	horaCitaCambiada,
	personaCambioCita,
	razonCambioCita,
	observacionCambio,
	mensajeContactoCita,
	fechaNuevoContacto,
	personaCancelaCita,
	motivoCancelaCita,
	fechaRetomarContactoCita,
	vozCliente,
	horaPosibleArribo,
	estadoReprogramar,
	horaInicio,
	horaEntrega,
	fechaCitaReprogramada,
	observacionReprogramacion,
	detalleIncumplimiento,
	estadoCita string,
	idServicio,
	idContacto,
	idMotivoResultado int64) *Cita {
	objcita := &Cita{
		Servicio:                  servicio,
		DetalleServicio:           detalleServicio,
		FechaCita:                 fechaCita,
		FechaPactadaCita:          fechaPactadaCita,
		MotivoCita:                motivoCita,
		Detallecita:               detallecita,
		ObservacionCita:           observacionCita,
		AclaracionRecordatorio:    aclaracionRecordatorio,
		FechaCitaCambiada:         fechaCitaCambiada,
		HoraCitaCambiada:          horaCitaCambiada,
		PersonaCambioCita:         personaCambioCita,
		RazonCambioCita:           razonCambioCita,
		ObservacionCambio:         observacionCambio,
		MensajeContactoCita:       mensajeContactoCita,
		FechaNuevoContacto:        fechaNuevoContacto,
		PersonaCancelaCita:        personaCancelaCita,
		MotivoCancelaCita:         motivoCancelaCita,
		FechaRetomarContactoCita:  fechaRetomarContactoCita,
		VozCliente:                vozCliente,
		HoraPosibleArribo:         horaPosibleArribo,
		EstadoReprogramar:         estadoReprogramar,
		HoraInicio:                horaInicio,
		HoraEntrega:               horaEntrega,
		FechaCitaReprogramada:     fechaCitaReprogramada,
		ObservacionReprogramacion: observacionReprogramacion,
		DetalleIncumplimiento:     detalleIncumplimiento,
		EstadoCita:                estadoCita,
		IdServicio:                idServicio,
		IdContacto:                idContacto,
		IdMotivoResultado:         idMotivoResultado,
	}
	return objcita
}

func CrearCita(
	servicio,
	detalleServicio,
	fechaCita,
	fechaPactadaCita,
	motivoCita,
	detallecita,
	observacionCita,
	aclaracionRecordatorio,
	fechaCitaCambiada,
	horaCitaCambiada,
	personaCambioCita,
	razonCambioCita,
	observacionCambio,
	mensajeContactoCita,
	fechaNuevoContacto,
	personaCancelaCita,
	motivoCancelaCita,
	fechaRetomarContactoCita,
	VozCliente,
	horaPosibleArribo,
	estadoReprogramar,
	horaInicio,
	horaEntrega,
	fechaCitaReprogramada,
	observacionReprogramacion,
	detalleIncumplimiento,
	estadoCita string,
	idServicio,
	idContacto,
	idMotivoResultado int64) *Cita {
	objcita := NuevaCita(
		servicio,
		detalleServicio,
		fechaCita,
		fechaPactadaCita,
		motivoCita,
		detallecita,
		observacionCita,
		aclaracionRecordatorio,
		fechaCitaCambiada,
		horaCitaCambiada,
		personaCambioCita,
		razonCambioCita,
		observacionCambio,
		mensajeContactoCita,
		fechaNuevoContacto,
		personaCancelaCita,
		motivoCancelaCita,
		fechaRetomarContactoCita,
		VozCliente,
		horaPosibleArribo,
		estadoReprogramar,
		horaInicio,
		horaEntrega,
		fechaCitaReprogramada,
		observacionReprogramacion,
		detalleIncumplimiento,
		estadoCita,
		idServicio,
		idContacto,
		idMotivoResultado)
	objcita.Guardar()
	return objcita
}

func (this *Cita) Guardar() {
	if this.IdCita == 0 {
		this.insertar()
	} else {
		this.actualizar()
	}
}

func (this *Cita) insertar() {
	sql := `INSERT x_tacita SET servicio=?,
								detalleServicio=?,
								fechaCita=?,
								fechaPactadaCita=?,
								motivoCita=?,
								detallecita=?,
								observacionCita=?,
								aclaracionRecordatorio=?,
								fechaCitaCambiada=?,
								horaCitaCambiada=?,
								personaCambioCita=?,
								razonCambioCita=?,
								observacionCambio=?,
								mensajeContactoCita=?,
								fechaNuevoContacto=?,
								personaCancelaCita=?,
								motivoCancelaCita=?,
								fechaRetomarContactoCita=?,
								vozCliente=?,
								horaPosibleArribo=?,
								estadoReprogramar=?,
								horaInicio=?,
								horaEntrega=?,
								fechaCitaReprogramada=?,
								observacionReprogramacion=?,
								detalleIncumplimiento=?,
								estadoCita=?,
								idServicio=?,
								idContacto=?,
								idMotivoResultado=?`
	fechaCita, err := time.Parse(time.RFC3339, this.FechaCita)
	fechaCitaP, err := time.Parse(time.RFC3339, this.FechaPactadaCita)
	fechaCitaC, err := time.Parse(time.RFC3339, this.FechaCitaCambiada)
	horaCitaC, err := time.Parse(time.RFC3339, this.HoraCitaCambiada)
	fechaNuevoC, _ := time.Parse(time.RFC3339, this.FechaNuevoContacto)
	fechaRetomaCC, _ := time.Parse(time.RFC3339, this.FechaRetomarContactoCita)
	horaPA, _ := time.Parse(time.RFC3339, this.HoraPosibleArribo)
	horaI, _ := time.Parse(time.RFC3339, this.HoraInicio)
	horaE, _ := time.Parse(time.RFC3339, this.HoraEntrega)
	fechaCitaR, _ := time.Parse(time.RFC3339, this.FechaCitaReprogramada)
	result, err := Exec(sql,
		this.Servicio,
		this.DetalleServicio,
		fechaCita,
		fechaCitaP,
		this.MotivoCita,
		this.Detallecita,
		this.ObservacionCita,
		this.AclaracionRecordatorio,
		fechaCitaC,
		horaCitaC,
		this.PersonaCambioCita,
		this.RazonCambioCita,
		this.ObservacionCambio,
		this.MensajeContactoCita,
		fechaNuevoC,
		this.PersonaCancelaCita,
		this.MotivoCancelaCita,
		fechaRetomaCC,
		this.VozCliente,
		horaPA,
		this.EstadoReprogramar,
		horaI,
		horaE,
		fechaCitaR,
		this.ObservacionReprogramacion,
		this.DetalleIncumplimiento,
		this.EstadoCita,
		this.IdServicio,
		this.IdContacto,
		this.IdMotivoResultado)
	this.IdCita, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
	} else {
		println("LastInsertId:", this.IdCita)
	}
}

func (this *Cita) actualizar() {
	sql := `UPDATE x_tacita SET servicio=?
								detalleServicio=?
								fechaCita=?,
								fechaPactadaCita=?,
								motivoCita=?,
								detallecita=?,
								observacionCita=?,
								aclaracionRecordatorio=?,
								fechaCitaCambiada=?,
								horaCitaCambiada=?,
								personaCambioCita=?,
								razonCambioCita=?,
								observacionCambio=?,
								mensajeContactoCita=?,
								fechaNuevoContacto=?,
								personaCancelaCita=?,
								motivoCancelaCita=?,
								fechaRetomarContactoCita=?,
								vozCliente=?,
								horaPosibleArribo=?,
								estadoReprogramar=?,
								horaInicio=?,
								horaEntrega=?,
								fechaCitaReprogramada=?,
								observacionReprogramacion=?,
								detalleIncumplimiento=?,
								estadoCita=?,
								idServicio=?,
								idContacto=?,
								idMotivoResultado=?`

	fechaCita, _ := time.Parse(time.RFC3339, this.FechaCita)
	fechaCitaP, _ := time.Parse(time.RFC3339, this.FechaPactadaCita)
	fechaCitaC, _ := time.Parse(time.RFC3339, this.FechaCitaCambiada)
	horaCitaC, _ := time.Parse(time.RFC3339, this.HoraCitaCambiada)
	fechaNuevoC, _ := time.Parse(time.RFC3339, this.FechaNuevoContacto)
	fechaRetomaCC, _ := time.Parse(time.RFC3339, this.FechaRetomarContactoCita)
	horaPA, _ := time.Parse(time.RFC3339, this.HoraPosibleArribo)
	horaI, _ := time.Parse(time.RFC3339, this.HoraInicio)
	horaE, _ := time.Parse(time.RFC3339, this.HoraEntrega)
	fechaCitaR, _ := time.Parse(time.RFC3339, this.FechaCitaReprogramada)
	Exec(sql,
		this.Servicio,
		this.DetalleServicio,
		fechaCita,
		fechaCitaP,
		this.MotivoCita,
		this.Detallecita,
		this.ObservacionCita,
		this.AclaracionRecordatorio,
		fechaCitaC,
		horaCitaC,
		this.PersonaCambioCita,
		this.RazonCambioCita,
		this.ObservacionCambio,
		this.MensajeContactoCita,
		fechaNuevoC,
		this.PersonaCancelaCita,
		this.MotivoCancelaCita,
		fechaRetomaCC,
		this.VozCliente,
		horaPA,
		this.EstadoReprogramar,
		horaI,
		horaE,
		fechaCitaR,
		this.ObservacionReprogramacion,
		this.DetalleIncumplimiento,
		this.EstadoCita,
		this.IdServicio,
		this.IdContacto,
		this.IdMotivoResultado)
}
