package models

type HistorialCont struct {
	Placa             string `json:"placa"`
	FechaRegistro     string `json:"fechaRegistro"`
	Cliente           string `json:"cliente"`
	ContactoCliente   string `json:"contactoCliente"`
	Estado            string `json:"estado"`
	Motivo            string `json:"motivo"`
	Activo            string `json:"activo"`
	ResultadoContacto string `json:"resultadoContacto"`
	SeLogroContacto   string `json:"seLogroContacto"`
	TrabajadorCorasur string `json:"trabajadorCorasur"`
}

type HistorialesCont []HistorialCont

func ContactoHistorialCont() HistorialesCont {
	sql := `SELECT x_tacontacto.placa
	, x_tacontacto.fechaRegistro
	, x_tacontacto.cliente
	, x_tacontacto.contactoCliente
	, x_taestado.estado	
	, x_tamotivo.motivo
	, x_tacontacto.activo
	, x_tacontacto.resultadoContacto	
	, x_tacontacto.trabajadorCorasur
	, x_tacontacto.seLogroContacto FROM x_tacontacto inner join x_tamotivo 
	on x_tacontacto.idMotivo= x_tamotivo.idMotivo inner join x_taestado 
	on x_tacontacto.idEstado = x_taestado.idEstado order by x_tacontacto.fechaRegistro desc
	LIMIT 100`
	// , x_tacontacto.seLogroContacto
	listado := HistorialesCont{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := HistorialCont{}
		rows.Scan(&obj.Placa,
			&obj.FechaRegistro,
			&obj.Cliente,
			&obj.ContactoCliente,
			&obj.Estado,
			&obj.Motivo,
			&obj.Activo,
			&obj.ResultadoContacto,
			&obj.TrabajadorCorasur,
			&obj.SeLogroContacto)
		listado = append(listado, obj)
	}
	return listado
}

func ContactoHistorialParametroCont(fecha1 string, fecha2 string) HistorialesCont {
	sql := `SELECT x_tacontacto.placa
	, x_tacontacto.fechaRegistro
	, x_tacontacto.cliente
	, x_tacontacto.contactoCliente
	, x_taestado.estado	
	, x_tamotivo.motivo
	, x_tacontacto.activo
	, x_tacontacto.resultadoContacto	
	, x_tacontacto.trabajadorCorasur
	, x_tacontacto.seLogroContacto FROM x_tacontacto inner join x_tamotivo 
	on x_tacontacto.idMotivo= x_tamotivo.idMotivo inner join x_taestado 
	on x_tacontacto.idEstado = x_taestado.idEstado WHERE x_tacontacto.fechaRegistro BETWEEN ? AND ? order by x_tacontacto.fechaRegistro desc`
	listado := HistorialesCont{}
	rows, _ := Query(sql, fecha1, fecha2)
	for rows.Next() {
		obj := HistorialCont{}
		rows.Scan(
			&obj.Placa,
			&obj.FechaRegistro,
			&obj.Cliente,
			&obj.ContactoCliente,
			&obj.Estado,
			&obj.Motivo,
			&obj.Activo,
			&obj.ResultadoContacto,
			&obj.TrabajadorCorasur,
			&obj.SeLogroContacto)
		listado = append(listado, obj)
	}
	return listado
}

func ContactoHistorialParametroPlaca(placa string) HistorialesCont {
	sql := `SELECT x_tacontacto.placa
	, x_tacontacto.fechaRegistro
	, x_tacontacto.cliente
	, x_tacontacto.contactoCliente
	, x_taestado.estado	
	, x_tamotivo.motivo
	, x_tacontacto.activo
	, x_tacontacto.resultadoContacto	
	, x_tacontacto.trabajadorCorasur
	, x_tacontacto.seLogroContacto FROM x_tacontacto inner join x_tamotivo 
	on x_tacontacto.idMotivo= x_tamotivo.idMotivo inner join x_taestado 
	on x_tacontacto.idEstado = x_taestado.idEstado WHERE x_tacontacto.placa=? order by x_tacontacto.fechaRegistro desc`
	listado := HistorialesCont{}
	rows, _ := Query(sql, placa)
	for rows.Next() {
		obj := HistorialCont{}
		rows.Scan(
			&obj.Placa,
			&obj.FechaRegistro,
			&obj.Cliente,
			&obj.ContactoCliente,
			&obj.Estado,
			&obj.Motivo,
			&obj.Activo,
			&obj.ResultadoContacto,
			&obj.TrabajadorCorasur,
			&obj.SeLogroContacto)
		listado = append(listado, obj)
	}
	return listado
}
