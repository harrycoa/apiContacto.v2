package models

type OrdenReparacion struct {
	Placa           string `json:"placa"`
	Numero          int64  `json:"numero"`
	Nombre          string `json:"nombre"`
	Km              int64  `json:"km"`
	Servicio        string `json:"servicio"`
	Telefono        string `json:"telefono"`
	FechaInicio     string `json:"fechaInicio"`
	FechaFin        string `json:"fechaFin"`
	Serie           string `json:"serie"`
	Motor           string `json:"motor"`
	Chasis          string `json:"chasis"`
	Vin             string `json:"vin"`
	AnioFabricacion string `json:"anioFabricacion"`
}

// OrdenReparaciones es un tipo
type OrdenReparaciones []OrdenReparacion

// ListarOrdenes lista ordenes de reparación
func ListarOrdenes(placa string) OrdenReparaciones {
	/*sql := `SELECT o.placa,
	  o.numero,
	  o.nombre,
	  o.km ,
	  c.c1,
	  o.celular,
	  o.fhinire,
	  o.fhfinen
	  FROM gx_oreparacion o inner join gx_citas c
	  on o.placa=c.placa where o.placa=?
	  group by o.numero` */

	sql := `select Ore.placa as placa,		
					Ore.numero as OrdenReparacion,  
					Ore.nombre as NombrePropietario,      
					Ore.km as kilomertraje,
					Ore.celular as Celular,
					Ore.ccodserv as Servicio,    
					Ore.fhinire as FechaRecepcion,
					Ore.fhfinen as FechaEntrega,     
					Ve.tdp as Serie,
					Ve.Motor as Motor,
					Ve.Chasis as Chasis,
					Ve.vin as vin,
					Ve.anofab as AñoFabricacion       
				from  gx_oreparacion as Ore inner join gx_vehiculo as Ve
				on Ore.placa=Ve.placa
				where Ore.placa=? and (Ore.idtipo='01' or Ore.idtipo='02')
				group by Ore.numero
				ORDER BY Ore.fhinire DESC;`
	listado := OrdenReparaciones{}
	rows, _ := Query(sql, placa)
	for rows.Next() {
		obj := OrdenReparacion{}
		rows.Scan(
			&obj.Placa,
			&obj.Numero,
			&obj.Nombre,
			&obj.Km,
			&obj.Telefono,
			&obj.Servicio,
			&obj.FechaInicio,
			&obj.FechaFin,
			&obj.Serie,
			&obj.Motor,
			&obj.Chasis,
			&obj.Vin,
			&obj.AnioFabricacion)
		listado = append(listado, obj)
	}
	return listado
}

// OrdenesPronostico devuelve las 2 últimas ordenes
func OrdenesPronostico(placa string) OrdenReparaciones {

	sql := `SELECT a.placa as placa
				, a.numero as OrdenReparacion
				, a.nombre as NombrePropietario
				, a.km as kilomertraje
				, a.celular as Celular
				, a.ccodserv as Servicio
				, date(a.fhinire) as FechaRecepcion
				, date(a.fhfinen) as FechaEntrega
				, b.tdp as Serie
				, b.Motor as Motor
				, b.Chasis as Chasis
				, b.vin as vin
				, b.anofab as AñoFabricacion
			FROM gx_oreparacion as a 
			INNER JOIN gx_vehiculo as b
				ON a.placa=b.placa
			WHERE a.placa=? and a.idtipo='01'
			GROUP BY a.numero
			ORDER BY a.fhinire DESC
			LIMIT 2;`

	ordenes := OrdenReparaciones{}
	rows, _ := Query(sql, placa)
	for rows.Next() {
		orden := OrdenReparacion{}
		rows.Scan(
			&orden.Placa,
			&orden.Numero,
			&orden.Nombre,
			&orden.Km,
			&orden.Telefono,
			&orden.Servicio,
			&orden.FechaInicio,
			&orden.FechaFin,
			&orden.Serie,
			&orden.Motor,
			&orden.Chasis,
			&orden.Vin,
			&orden.AnioFabricacion)
		ordenes = append(ordenes, orden)
	}

	if len(ordenes) == 0 {
		orden := OrdenReparacion{}
		ordenes = append(ordenes, orden)
		ordenes = append(ordenes, orden)
	} else if len(ordenes) < 2 {
		orden := ordenes[0]
		ordenes = append(ordenes, orden)
	}
	return ordenes
}
