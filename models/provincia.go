package models

type Provincia struct {
	IdProvincia    int64  `json:"idProvincia"`
	Provincia      string `json:"provincia"`
	IdDepartamento int64
}

const provinciaEsquema string = `CREATE TABLE x_taprovincia (
  idProvincia int(11) NOT NULL AUTO_INCREMENT,
  provincia  varchar(200) NULL,
  idDepartamento int(11),
  PRIMARY KEY (idProvincia)
);`

type Provincias []Provincia

// constructores
func ListarProvincias() Provincias {
	sql := "SELECT idProvincia, provincia, idDepartamento FROM x_taprovincia"
	listado := Provincias{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Provincia{}
		rows.Scan(&obj.IdProvincia, &obj.Provincia, &obj.IdDepartamento)
		listado = append(listado, obj)
	}
	return listado
}

func ListarProvincia(idDepartamento string) Provincias {
	sql := "SELECT idProvincia, provincia, idDepartamento FROM x_taprovincia WHERE idDepartamento=?"
	listado := Provincias{}
	rows, _ := Query(sql, idDepartamento)
	for rows.Next() {
		obj := Provincia{}
		rows.Scan(&obj.IdProvincia, &obj.Provincia, &obj.IdDepartamento)
		listado = append(listado, obj)
	}
	return listado
}
