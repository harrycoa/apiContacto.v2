package models

type GestionNegativaConsulta struct {
	IdContacto        int64  `json:"idContacto"`
	TrabajadorCorasur string `json:"trabajadorCorasur"`
	Placa             string `json:"placa"`
	Cliente           string `json:"cliente"`
	ContactoCliente   string `json:"contactoCliente"`
	ResultadoContacto string `json:"resultadoContacto"`
	Motivo            string `json:"motivo"`
	VozCliente        string `json:"vozCliente"`
	FechaRegistro     string `json:"fechaRegistro"`
}

const GestionNegativaConsultaEsquema string = `select idContacto,
		trabajadorCorasur,
		placa,
		Cliente,
		contactoCliente,
		resultadoContacto,
		m.motivo,
		vozCliente,
		fechaRegistro
		from x_tacontacto c inner join x_tamotivo m
		on c.idMotivo=m.idMotivo
		where activo='S'`

type GestionNegativaConsultas []GestionNegativaConsulta

func ListarGestionNegativaConsultas() GestionNegativaConsultas {
	sql := `select idContacto,
						trabajadorCorasur,
						placa,
						Cliente,
						contactoCliente,
						resultadoContacto,
						m.motivo,
						vozCliente,
						fechaRegistro
						from x_tacontacto c inner join x_tamotivo m
						on c.idMotivo=m.idMotivo
						where activo='S' and estadoContacto='N' `
	listado := GestionNegativaConsultas{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := GestionNegativaConsulta{}
		rows.Scan(&obj.IdContacto,
			&obj.TrabajadorCorasur,
			&obj.Placa,
			&obj.Cliente,
			&obj.ContactoCliente,
			&obj.ResultadoContacto,
			&obj.Motivo,
			&obj.VozCliente,
			&obj.FechaRegistro)
		listado = append(listado, obj)
	}
	return listado
}

func ListarGestionNegativaConsulta(placa string) GestionNegativaConsultas {
	sql := `select idContacto,
					trabajadorCorasur,
					placa,
					Cliente,
					contactoCliente,
					resultadoContacto,
					m.motivo,
					vozCliente,
					fechaRegistro
					from x_tacontacto c inner join x_tamotivo m
					on c.idMotivo=m.idMotivo
					where activo='S' and estadoContacto='N'`
	listado := GestionNegativaConsultas{}
	rows, _ := Query(sql, placa)
	for rows.Next() {
		obj := GestionNegativaConsulta{}
		rows.Scan(&obj.IdContacto,
			&obj.TrabajadorCorasur,
			&obj.Placa,
			&obj.Cliente,
			&obj.ContactoCliente,
			&obj.ResultadoContacto,
			&obj.Motivo,
			&obj.VozCliente,
			&obj.FechaRegistro)
		listado = append(listado, obj)
	}
	return listado
}
