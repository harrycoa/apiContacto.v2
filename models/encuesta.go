package models

type Encuesta struct {
	IdEncuesta     int64  `json:"idEncuesta"`
	EstadoEncuenta string `json:"estadoEncuenta"`
	VozCliente     string `json:"vozCliente"`
	Sugerencia     string `json:"sugerencia"`
	IdServicio     int64
	IdPregunta     int64
}

const encuestaEsquema string = `CREATE TABLE x_taencuesta (
	idEncuesta int(11) NOT NULL AUTO_INCREMENT,
	estadoEncuenta char(1) DEFAULT NULL,
	vozCliente varchar(100) DEFAULT NULL,
	sugerencia varchar(200) DEFAULT NULL,
	idServicio int(11) DEFAULT NULL,
	idPregunta int(11) DEFAULT NULL,
	PRIMARY KEY (idEncuesta)
  );`

type Encuestas []Encuesta

func ListarEncuestas() Encuestas {
	sql := `SELECT idEncuesta, 
				estadoEncuenta, 
				vozCliente, 			
				sugerencia, 
				idServicio, 
				idPregunta FROM x_taencuesta`
	listado := Encuestas{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Encuesta{}
		rows.Scan(&obj.IdEncuesta,
			&obj.EstadoEncuenta,
			&obj.VozCliente,
			&obj.Sugerencia,
			&obj.IdServicio,
			&obj.IdPregunta)
		listado = append(listado, obj)
	}
	return listado
}

func ListarEncuesta(estadoEncuesta string) Encuestas {
	sql := `SELECT idEncuesta, 
				estadoEncuenta, 
				vozCliente, 
				sugerencia, 
				idServicio, 
				idPregunta FROM x_taencuesta where estadoEncuesta=?`
	listado := Encuestas{}
	rows, _ := Query(sql, estadoEncuesta)
	for rows.Next() {
		obj := Encuesta{}
		rows.Scan(&obj.IdEncuesta,
			&obj.EstadoEncuenta,
			&obj.VozCliente,
			&obj.Sugerencia,
			&obj.IdServicio,
			&obj.IdPregunta)
		listado = append(listado, obj)
	}
	return listado
}

func NuevaEncuesta(
	estadoEncuesta,
	vozCliente,
	sugerencia string,
	idServicio,
	idPregunta int64) *Encuesta {
	obj := &Encuesta{
		EstadoEncuenta: estadoEncuesta,
		VozCliente:     vozCliente,
		Sugerencia:     sugerencia,
		IdServicio:     idServicio,
		IdPregunta:     idPregunta,
	}
	return obj
}

func CrearEncuesta(
	estadoEncuesta,
	vozCliente,
	sugerencia string,
	idServicio,
	idPregunta int64) *Encuesta {
	obj := NuevaEncuesta(
		estadoEncuesta,
		vozCliente,
		sugerencia,
		idServicio,
		idPregunta)
	obj.Guardar()
	return obj
}

func (this *Encuesta) Guardar() {
	if this.IdEncuesta == 0 {
		this.insertar()
	}
}

func (this *Encuesta) insertar() {
	sql := `INSERT x_taencuesta SET estadoEncuesta=?, 
											vozCliente=?, 									
											sugerencia=?, 											
											idServicio=?, 
											idPregunta=?`
	result, err := Exec(sql,
		this.EstadoEncuenta,
		this.VozCliente,
		this.Sugerencia,
		this.IdServicio,
		this.IdPregunta)
	this.IdEncuesta, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
	} else {
		println("LastInsertId:", this.IdEncuesta)
	}
}
