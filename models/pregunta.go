package models

type Pregunta struct {
	IdPregunta int64  `json:"idPregunta"`
	Pregunta   string `json:"pregunta"`
	Clase      string `json:"clase"`
}

const preguntaEsquema string = `CREATE TABLE x_tapregunta (
	idPregunta int(11) NOT NULL AUTO_INCREMENT,
	pregunta varchar(200),
	clase char(1) NULL,  
	PRIMARY KEY (idPregunta)
  );`

type Preguntas []Pregunta

// constructores
func ListarPreguntas() Preguntas {
	sql := "SELECT idPregunta, pregunta, clase FROM x_tapregunta"
	listado := Preguntas{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Pregunta{}
		rows.Scan(&obj.IdPregunta, &obj.Pregunta, &obj.Clase)
		listado = append(listado, obj)
	}
	return listado
}

func ListarPregunta(clase string) Preguntas {
	sql := "SELECT idPregunta, pregunta, clase FROM x_tapregunta WHERE clase=?"
	listado := Preguntas{}
	rows, _ := Query(sql, clase)
	for rows.Next() {
		obj := Pregunta{}
		rows.Scan(&obj.IdPregunta, &obj.Pregunta, &obj.Clase)
		listado = append(listado, obj)
	}
	return listado
}
