package models

type BusquedaPlaca struct {
	Placa           string `json:"serie"`
	Vin             string `json:"vin"`
	AnioFabricacion string `json:"anioFabricacion"`
	Modelo          string `json:"modelo"`
	Cliente         string `json:"cliente"`
	Telefono        string `json:"telefono"`
}

type BusquedaPlacas []BusquedaPlaca

func PlacaBusqueda(placa1 string) BusquedaPlacas {
	sql := `select Ve.placa as placa,	
					Ve.vin as vin,
					Ve.anofab as anioFabricacion, 
					Ve.modelo as modelo,   
					Ore.nombre as cliente,
					Ore.celular as telefono				   
					from  gx_oreparacion as Ore inner join gx_vehiculo as Ve
					on Ore.placa=Ve.placa
					where Ore.placa=?
					group by Ore.placa;`
	listado := BusquedaPlacas{}
	rows, _ := Query(sql, placa1)
	for rows.Next() {
		obj := BusquedaPlaca{}
		rows.Scan(&obj.Placa, &obj.Vin, &obj.AnioFabricacion, &obj.Modelo, &obj.Cliente, &obj.Telefono)
		listado = append(listado, obj)
	}
	return listado
}
