package models

import "time"

type Presupuesto struct {
	IdPresupuesto      int64   `json:"idPresupuesto"`
	NumeroPresupuesto  int64   `json:"numeroPresupuesto"`
	MontoPresupuesto   float64 `json:"montoPresupuesto"`
	FechaProgramada    string  `json:"fechaProgramada"`
	VozCliente         string  `json:"vozCliente"`
	MotivoAceptacion   string  `json:"motivoAceptacion"`
	MotivoNoAceptacion string  `json:"motivoNoAceptacion"`
	IdMotivoResultado  int64
}

const presupuestoEsquema string = `CREATE TABLE x_tapresupuesto (
	idPresupuesto int(11) NOT NULL AUTO_INCREMENT,
	numeroPresupuesto int(10) DEFAULT NULL,
	montoPresupuesto double DEFAULT NULL,
	fechaProgramada datetime DEFAULT NULL,
	vozCliente varchar(200) DEFAULT NULL,
	motivoAceptacion varchar(100) DEFAULT NULL,
	motivoNoAceptacion varchar(100) DEFAULT NULL,
	idMotivoResultado int(11) DEFAULT NULL,
	PRIMARY KEY (idPresupuesto)
  );`

type Presupuestos []Presupuesto

func ListarPresupuestos() Presupuestos {
	sql := `SELECT idPresupuesto, 
					numeroPresupuesto, 
					montoPresupuesto, 
					fechaProgramada, 
					vozCliente, 		
					motivoAceptacion, 
					motivoNoAceptacion, 
					idMotivoResultado 
					FROM x_tapresupuesto`
	listado := Presupuestos{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Presupuesto{}
		rows.Scan(&obj.IdPresupuesto,
			&obj.NumeroPresupuesto,
			&obj.MontoPresupuesto,
			&obj.FechaProgramada,
			&obj.VozCliente,
			&obj.MotivoAceptacion,
			&obj.MotivoNoAceptacion,
			&obj.IdMotivoResultado)
		listado = append(listado, obj)
	}
	return listado
}

func ListarPresupuesto(numeroPresupuesto string) Presupuestos {
	sql := `SELECT idPresupuesto, 
					numeroPresupuesto, 
					montoPresupuesto, 
					fechaProgramada, 
					VozCliente, 
					motivoAceptacion, 
					motivoNoAceptacion, 
					idMotivoResultado 
					FROM x_tapresupuesto where numeroPresupuesto=?`
	listado := Presupuestos{}
	rows, _ := Query(sql, numeroPresupuesto)
	for rows.Next() {
		obj := Presupuesto{}
		rows.Scan(&obj.IdPresupuesto,
			&obj.NumeroPresupuesto,
			&obj.MontoPresupuesto,
			&obj.FechaProgramada,
			&obj.VozCliente,
			&obj.MotivoAceptacion,
			&obj.MotivoNoAceptacion,
			&obj.IdMotivoResultado)
		listado = append(listado, obj)
	}
	return listado
}

func NuevoPresupuesto(
	numeroPresupuesto int64,
	montoPresupuesto float64,
	fechaProgramada,
	vozCliente,
	motivoAceptacion,
	motivoNoAceptacion string,
	idMotivoResultado int64) *Presupuesto {
	obj := &Presupuesto{
		NumeroPresupuesto:  numeroPresupuesto,
		MontoPresupuesto:   montoPresupuesto,
		FechaProgramada:    fechaProgramada,
		VozCliente:         vozCliente,
		MotivoAceptacion:   motivoAceptacion,
		MotivoNoAceptacion: motivoNoAceptacion,
		IdMotivoResultado:  idMotivoResultado,
	}
	return obj
}

func CrearPresupuesto(
	numeroPresupuesto int64,
	montoPresupuesto float64,
	fechaProgramada,
	vozCliente,
	motivoAceptacion,
	motivoNoAceptacion string,
	idMotivoResultado int64) *Presupuesto {
	obj := NuevoPresupuesto(
		numeroPresupuesto,
		montoPresupuesto,
		fechaProgramada,
		vozCliente,
		motivoAceptacion,
		motivoNoAceptacion,
		idMotivoResultado)
	obj.Guardar()
	return obj
}

func (this *Presupuesto) Guardar() {
	if this.IdPresupuesto == 0 {
		this.insertar()
	}
}

func (this *Presupuesto) insertar() {
	sql := `INSERT x_tapresupuesto SET numeroPresupuesto=?, 
										montoPresupuesto=?, 
										fechaProgramada=?, 
										vozCliente=?, 								
										motivoAceptacion=?, 
										motivoNoAceptacion=?, 
										idMotivoResultado=?`
	fechaProg, _ := time.Parse(time.RFC3339, this.FechaProgramada)
	result, err := Exec(sql,
		this.NumeroPresupuesto,
		this.MontoPresupuesto,
		fechaProg,
		this.VozCliente,
		this.MotivoAceptacion,
		this.MotivoNoAceptacion,
		this.IdMotivoResultado)
	this.IdPresupuesto, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
	} else {
		println("LastInsertId:", this.IdPresupuesto)
	}
}
