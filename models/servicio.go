package models

type Servicio struct {
	IdServicio      int64  `json:"idServicio"`
	TipoServicio    string `json:"tipoServicio"`
	ClaseServicio   string `json:"claseServicio"`
	Servicio        string `json:"servicio"`
	DetalleServicio string `json:"detalleServicio"`
	KmMantenimiento int64  `json:"kmMantenimiento"`
}

const servicioEsquema string = `CREATE TABLE x_taservicio (
	idServicio int(11) NOT NULL AUTO_INCREMENT,
	tipoServicio varchar(100) DEFAULT NULL,
	claseServicio varchar(100) DEFAULT NULL,
	servicio varchar(100) DEFAULT NULL,
	detalleServicio varchar(200) DEFAULT NULL,
	kmMantenimiento int(100) DEFAULT NULL,
	PRIMARY KEY (idServicio)
  );`

type Servicios []Servicio

func ListarServicios() Servicios {
	sql := "SELECT idServicio, tipoServicio, claseServicio, servicio, detalleServicio, kmMantenimiento FROM x_taservicio"
	listado := Servicios{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Servicio{}
		rows.Scan(&obj.IdServicio, &obj.TipoServicio, &obj.ClaseServicio, &obj.Servicio, &obj.DetalleServicio, &obj.KmMantenimiento)
		listado = append(listado, obj)
	}
	return listado
}

func ListarServicio(claseServicio string) Servicios {
	sql := "SELECT idServicio, tipoServicio, claseServicio, servicio, detalleServicio, kmMantenimiento FROM x_taservicio WHERE claseServicio=?"
	listado := Servicios{}
	rows, _ := Query(sql, claseServicio)
	for rows.Next() {
		obj := Servicio{}
		rows.Scan(&obj.IdServicio, &obj.TipoServicio, &obj.ClaseServicio, &obj.Servicio, &obj.DetalleServicio, &obj.KmMantenimiento)
		listado = append(listado, obj)
	}
	return listado
}

func NuevoServicio(
	tipoServicio,
	claseServicio,
	servicio,
	detalleServicio string,
	kmMantenimiento int64) *Servicio {
	objservicio := &Servicio{
		TipoServicio:    tipoServicio,
		ClaseServicio:   claseServicio,
		Servicio:        servicio,
		DetalleServicio: detalleServicio,
		KmMantenimiento: kmMantenimiento,
	}
	return objservicio
}

func CrearServicio(
	tipoServicio,
	claseServicio,
	servicio,
	detalleServicio string,
	kmMantenimiento int64) *Servicio {
	objservicio := NuevoServicio(
		tipoServicio,
		claseServicio,
		servicio,
		detalleServicio,
		kmMantenimiento)
	objservicio.Guardar()
	return objservicio
}

func (this *Servicio) Guardar() {
	if this.IdServicio == 0 {
		this.insertar()
	} else {
		this.actualizar()
	}
}

func (this *Servicio) insertar() {
	sql := `INSERT x_taservicio SET tipoServicio=?, 
									claseServicio=?
									servicio=?, 
									detalleServicio=?, 
									kmMantenimiento=?`
	result, err := Exec(sql,
		this.TipoServicio,
		this.ClaseServicio,
		this.Servicio,
		this.DetalleServicio,
		this.KmMantenimiento)
	this.IdServicio, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
	} else {
		println("LastInsertId:", this.IdServicio)
	}
}

func (this *Servicio) actualizar() {
	sql := `UPDATE x_taservicio SET tipoServicio=?, 
									claseServicio=?
									servicio=?, 
									detalleServicio=?, 
									kmMantenimiento=?`
	Exec(sql,
		this.TipoServicio,
		this.ClaseServicio,
		this.Servicio,
		this.DetalleServicio,
		this.KmMantenimiento)
}
