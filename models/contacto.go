package models

import "time"

type Contacto struct {
	IdContacto                int64  `json:"idContacto"`
	TrabajadorCorasur         string `json:"trabajadorCorasur"`
	TipoContactoCorasur       string `json:"tipoContactoCorasur"`
	ContactoCorasur           string `json:"contactoCorasur"`
	Placa                     string `json:"placa"`
	IdVehiculo                int64
	Cliente                   string `json:"cliente"`
	TipoContactoCliente       string `json:"tipoContactoCliente"`
	ContactoCliente           string `json:"contactoCliente"`
	Activo                    string `json:"activo"`
	ResultadoContacto         string `json:"resultadoContacto"`
	FechaRetomarContacto      string `json:"fechaRetomarContacto"`
	UltimoKilometrajeServicio int64  `json:"ultimoKilometrajeServicio"`
	FechaRegistro             string `json:"fechaRegistro"`
	IdMotivo                  int64
	IdEstado                  int64
	IdRespuesta               int64
	ManttoK                   int64  `json:"manttoK"`
	NuevoDuenio               string `json:"nuevoDuenio"`
	TelefonoNuevoDuenio       string `json:"telefonoNuevoDuenio"`
	AgendaCita                string `json:"agendaCita"`
	EstadoContacto            string `json:"estadoContacto"`
	VozCliente                string `json:"vozCliente"`
	FechaCitaReferencial1     string `json:"fechaCitaReferencial1"`
	DocumentoRecojo           string `json:"documentoRecojo"`
	FechaRecojoDocumento      string `json:"fechaRecojoDocumento"`
	Otro                      string `json:"otro"`
	IdNoCita                  int64
	Motivo                    string `json:"motivo"`
	Estado                    string `json:"estado"`
	Respuesta                 string `json:"Respuesta"`

	IdPresupuesto   int64 `json:"idPresupuesto"`
	IdOrdenTrabajo  int64 `json:"idOrdenTrabajo"`
	IdPostergacion  int64 `json:"idPostergacion"`
	IdEncuesta      int64 `json:"idEncuesta"`
	IdSeguimiento   int64 `json:"idSeguimiento"`
	IdRecomendacion int64 `json:"idRecomendacion"`

	MotivoDetalle            string `json:"motivoDetalle"`
	MotivoDetalleDescripcion string `json:"motivoDetalleDescripcion"`
}

type Contactos []Contacto

const contactoEsquema string = `CREATE TABLE x_tacontacto (
  idContacto int(11) NOT NULL AUTO_INCREMENT,
  trabajadorCorasur varchar(200) NOT NULL,
  tipoContactoCorasur varchar(100) NOT NULL,
  contactoCorasur varchar(100) NOT NULL,
  placa varchar(6) NOT NULL,
  idVehiculo int(11) DEFAULT NULL,
  cliente varchar(200) NOT NULL,
  tipoContactoCliente varchar(100) NOT NULL,
  contactoCliente varchar(100) NOT NULL,
  activo char(1) NOT NULL,
  resultadoContacto varchar(200) DEFAULT NULL,
  fechaRetomarContacto datetime DEFAULT NULL,
  ultimoKilometrajeServicio int(100) DEFAULT NULL,
  fechaRegistro datetime NOT NULL,
  idMotivo int(11) DEFAULT NULL,
  idEstado int(11) DEFAULT NULL,
  idRespuesta int(11) DEFAULT NULL,
  manttoK int(100) DEFAULT NULL,
  nuevoDuenio varchar(200) DEFAULT NULL,
  telefonoNuevoDuenio varchar(100) DEFAULT NULL,
  agendaCita char(1) NOT NULL,
  estadoContacto char(1) DEFAULT NULL,
  vozCliente varchar(200) DEFAULT NULL,
  fechaCitaReferencial1 datetime DEFAULT NULL,
  documentoRecojo varchar(100) DEFAULT NULL,
  fechaRecojoDocumento datetime DEFAULT NULL,
  idNoCita int(11) DEFAULT NULL,
  PRIMARY KEY (idContacto)
);
`

// Constructores
func NuevoContacto(trabajadorCorasur,
	tipoContactoCorasur,
	contactoCorasur,
	placa string,
	idVehiculo int64,
	cliente,
	tipoContactoCliente,
	contactoCliente,
	activo,
	resultadoContacto,
	fechaRetomarContacto string,
	ultimoKilometrajeServicio int64,
	fechaRegistro string,
	idMotivo,
	idEstado,
	idRespuesta,
	manttoK int64,
	nuevoDuenio,
	telefonoNuevoDuenio,
	agendaCita,
	estadoContacto,
	vozCliente,
	fechaCitaReferencial1,
	documentoRecojo,
	fechaRecojoDocumento,
	otro string,
	idNoCita,
	idPresupuesto,
	idOrdenTrabajo,
	idPostergacion,
	idEncuesta,
	idSeguimiento,
	idRecomendacion int64,
	motivoDetalle,
	motivoDetalleDescripcion string) *Contacto {
	obj := &Contacto{TrabajadorCorasur: trabajadorCorasur,
		TipoContactoCorasur:      tipoContactoCorasur,
		ContactoCorasur:          contactoCorasur,
		Placa:                    placa,
		IdVehiculo:               idVehiculo,
		Cliente:                  cliente,
		TipoContactoCliente:      tipoContactoCliente,
		ContactoCliente:          contactoCliente,
		Activo:                   activo,
		ResultadoContacto:        resultadoContacto,
		FechaRetomarContacto:     fechaRetomarContacto,
		FechaRegistro:            fechaRegistro,
		IdMotivo:                 idMotivo,
		IdEstado:                 idEstado,
		IdRespuesta:              idRespuesta,
		ManttoK:                  manttoK,
		NuevoDuenio:              nuevoDuenio,
		TelefonoNuevoDuenio:      telefonoNuevoDuenio,
		AgendaCita:               agendaCita,
		EstadoContacto:           estadoContacto,
		VozCliente:               vozCliente,
		FechaCitaReferencial1:    fechaCitaReferencial1,
		DocumentoRecojo:          documentoRecojo,
		FechaRecojoDocumento:     fechaRecojoDocumento,
		Otro:                     otro,
		IdNoCita:                 idNoCita,
		IdPresupuesto:            idPresupuesto,
		IdOrdenTrabajo:           idOrdenTrabajo,
		IdPostergacion:           idPostergacion,
		IdEncuesta:               idEncuesta,
		IdSeguimiento:            idSeguimiento,
		MotivoDetalle:            motivoDetalle,
		MotivoDetalleDescripcion: motivoDetalleDescripcion,
	}
	return obj
}

func CrearContacto(trabajadorCorasur,
	tipoContactoCorasur,
	contactoCorasur,
	placa string,
	idVehiculo int64,
	cliente,
	tipoContactoCliente,
	contactoCliente,
	activo,
	resultadoContacto,
	fechaRetomarContacto string,
	ultimoKilometrajeServicio int64,
	fechaRegistro string,
	idMotivo,
	idEstado,
	idRespuesta,
	manttoK int64,
	nuevoDuenio,
	telefonoNuevoDuenio,
	agendaCita,
	estadoContacto,
	vozCliente,
	fechaCitaReferencial1,
	documentoRecojo,
	fechaRecojoDocumento,
	otro string,
	idNoCita,
	idPresupuesto,
	idOrdenTrabajo,
	idPostergacion,
	idEncuesta,
	idSeguimiento,
	idRecomendacion int64,
	motivoDetalle,
	motivoDetalleDescripcion string) *Contacto {
	obj := NuevoContacto(trabajadorCorasur,
		tipoContactoCorasur,
		contactoCorasur,
		placa,
		idVehiculo,
		cliente,
		tipoContactoCliente,
		contactoCliente,
		activo,
		resultadoContacto,
		fechaRetomarContacto,
		ultimoKilometrajeServicio,
		fechaRegistro,
		idMotivo,
		idEstado,
		idRespuesta,
		manttoK,
		nuevoDuenio,
		telefonoNuevoDuenio,
		agendaCita,
		estadoContacto,
		vozCliente,
		fechaCitaReferencial1,
		documentoRecojo,
		fechaRecojoDocumento,
		otro,
		idNoCita,
		idPresupuesto,
		idOrdenTrabajo,
		idPostergacion,
		idEncuesta,
		idSeguimiento,
		idRecomendacion,
		motivoDetalle,
		motivoDetalleDescripcion)
	obj.Guardar()
	return obj
}

func ListarContactos() Contactos {
	sql := `SELECT idcontacto,
	trabajadorCorasur,
	tipoContactoCorasur,
	contactoCorasur,
	placa,
	cliente,
	tipoContactoCliente,
	contactoCliente,
	activo, 
	resultadoContacto, 
	fechaRetomarContacto,
	fecharegistro, 	
	x_tamotivo.motivo, 
	x_taestado.estado	
	FROM x_tacontacto inner join x_tamotivo 
	on x_tacontacto.idMotivo= x_tamotivo.idMotivo inner join x_taestado 
	on x_tacontacto.idEstado = x_taestado.idEstado`
	listado := Contactos{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Contacto{}
		rows.Scan(&obj.IdContacto,
			&obj.TrabajadorCorasur,
			&obj.TipoContactoCorasur,
			&obj.ContactoCorasur,
			&obj.Placa,
			&obj.Cliente,
			&obj.TipoContactoCliente,
			&obj.ContactoCliente,
			&obj.Activo,
			&obj.ResultadoContacto,
			&obj.FechaRetomarContacto,
			&obj.FechaRegistro,
			&obj.Motivo,
			&obj.Estado)
		listado = append(listado, obj)
	}
	return listado
}

func ListarContacto(id int) *Contacto {
	obj := NuevoContacto("", "", "", "", 0, "", "", "", "", "", "", 0, "", 0, 0, 0, 0, "", "", "", "", "", "", "", "", "", 0, 0, 0, 0, 0, 0, 0, "", "")
	sql := `SELECT idContacto,
					trabajadorCorasur,
					tipoContactoCorasur,
					contactoCorasur,
					placa,
					idVehiculo,
					cliente,
					tipoContactoCliente,
					contactoCliente,
					activo,
					resultadoContacto,
					fechaRetomarContacto,
					ultimoKilometrajeServicio,
					fechaRegistro,
					idMotivo,
					idEstado,
					idRespuesta,
					manttoK,
					nuevoDuenio,
					telefonoNuevoDuenio,
					agendaCita,
					estadoContacto,
					vozCliente,	
					fechaCitaReferencial1,			
					documentoRecojo,
					fechaRecojoDocumento,
					otro,
					idNoCita,
				   FROM x_tacontacto where idContacto=?`
	rows, _ := Query(sql, id)
	for rows.Next() {
		rows.Scan(&obj.IdContacto,
			&obj.TrabajadorCorasur,
			&obj.TipoContactoCorasur,
			&obj.ContactoCorasur,
			&obj.Placa,
			&obj.IdVehiculo,
			&obj.Cliente,
			&obj.TipoContactoCliente,
			&obj.ContactoCliente,
			&obj.Activo,
			&obj.ResultadoContacto,
			&obj.FechaRetomarContacto,
			&obj.UltimoKilometrajeServicio,
			&obj.FechaRegistro,
			&obj.IdMotivo,
			&obj.IdEstado,
			&obj.IdRespuesta,
			&obj.ManttoK,
			&obj.NuevoDuenio,
			&obj.TelefonoNuevoDuenio,
			&obj.AgendaCita,
			&obj.EstadoContacto,
			&obj.VozCliente,
			&obj.FechaCitaReferencial1,
			&obj.DocumentoRecojo,
			&obj.FechaRecojoDocumento,
			&obj.Otro,
			&obj.IdNoCita)
	}
	return obj
}

func HistorialContactos() Contactos {
	sql := `SELECT c.idContacto,
					c.trabajadorCorasur,
					c.tipoContactoCorasur,
					c.contactoCorasur,
					c.placa,
					c.idVehiculo,
					c.cliente,
					c.tipoContactoCliente,
					c.contactoCliente,
					c.activo,
					c.resultadoContacto,
					c.fechaRetomarContacto,
					c.ultimoKilometrajeServicio,
					c.fechaRegistro,					
					c.manttoK,
					c.nuevoDuenio,
					c.telefonoNuevoDuenio,
					c.agendaCita,
					c.estadoContacto,
					c.vozCliente,				
					c.fechaCitaReferencial1,			
					c.documentoRecojo,
					c.fechaRecojoDocumento,
					c.otro,
					c.idNoCita,
				   m.motivo, 
				   e.estado 		
				   FROM x_tacontacto as c inner join x_tamotivo as m
				   on c.idMotivo= m.idMotivo inner join x_taestado as e
				   on c.idEstado = e.idEstado`
	listado := Contactos{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Contacto{}
		rows.Scan(&obj.IdContacto,
			&obj.TrabajadorCorasur,
			&obj.TipoContactoCorasur,
			&obj.ContactoCorasur,
			&obj.Placa,
			&obj.IdVehiculo,
			&obj.Cliente,
			&obj.TipoContactoCliente,
			&obj.ContactoCliente,
			&obj.Activo,
			&obj.ResultadoContacto,
			&obj.FechaRetomarContacto,
			&obj.UltimoKilometrajeServicio,
			&obj.FechaRegistro,
			&obj.ManttoK,
			&obj.NuevoDuenio,
			&obj.TelefonoNuevoDuenio,
			&obj.AgendaCita,
			&obj.EstadoContacto,
			&obj.VozCliente,
			&obj.FechaCitaReferencial1,
			&obj.DocumentoRecojo,
			&obj.FechaRecojoDocumento,
			&obj.Otro,
			&obj.IdNoCita,
			&obj.Motivo,
			&obj.Estado)
		listado = append(listado, obj)
	}
	return listado
}
func ListarContactosParametro(placa string) Contactos {
	sql := `SELECT  idContacto,
					trabajadorCorasur,
					tipoContactoCorasur,
					contactoCorasur,
					placa,
					idVehiculo,
					cliente,
					tipoContactoCliente,
					contactoCliente,
					activo,
					resultadoContacto,
					fechaRetomarContacto,
					ultimoKilometrajeServicio,
					fechaRegistro,
					idMotivo,
					idEstado,
					idRespuesta,
					manttoK,
					nuevoDuenio,
					telefonoNuevoDuenio,
					agendaCita,
					estadoContacto,
					vozCliente,		
					fechaCitaReferencial1,
					documentoRecojo,
					fechaRecojoDocumento,
					otro,
					idNoCita,
				   FROM x_tacontacto where placa=?`
	listado := Contactos{}
	rows, _ := Query(sql, placa)
	for rows.Next() {
		obj := Contacto{}
		rows.Scan(&obj.IdContacto,
			&obj.TrabajadorCorasur,
			&obj.TipoContactoCorasur,
			&obj.ContactoCorasur,
			&obj.Placa,
			&obj.IdVehiculo,
			&obj.Cliente,
			&obj.TipoContactoCliente,
			&obj.ContactoCliente,
			&obj.Activo,
			&obj.ResultadoContacto,
			&obj.FechaRetomarContacto,
			&obj.UltimoKilometrajeServicio,
			&obj.FechaRegistro,
			&obj.IdMotivo,
			&obj.IdEstado,
			&obj.IdRespuesta,
			&obj.ManttoK,
			&obj.NuevoDuenio,
			&obj.TelefonoNuevoDuenio,
			&obj.AgendaCita,
			&obj.EstadoContacto,
			&obj.VozCliente,
			&obj.FechaCitaReferencial1,
			&obj.DocumentoRecojo,
			&obj.FechaRecojoDocumento,
			&obj.Otro,
			&obj.IdNoCita)
		listado = append(listado, obj)
	}
	return listado
}
func (this *Contacto) Guardar() {
	if this.IdContacto == 0 {
		this.insertar()
	} else {
		this.actualizar()
	}
}

func (this *Contacto) insertar() {
	sql := `INSERT x_tacontacto SET
					trabajadorCorasur=?,
					tipoContactoCorasur=?,
					contactoCorasur=?,
					placa=?,
					idVehiculo=?,
					cliente=?,
					tipoContactoCliente=?,
					contactoCliente=?,
					activo=?,
					resultadoContacto=?,
					fechaRetomarContacto=?,
					ultimoKilometrajeServicio=?,
					fechaRegistro=?,
					idMotivo=?,
					idEstado=?,
					idRespuesta=?,
					manttoK=?,
					nuevoDuenio=?,
					telefonoNuevoDuenio=?,
					agendaCita=?,
					estadoContacto=?,
					vozCliente=?,
					fechaCitaReferencial1=?,				
					documentoRecojo=?,
					fechaRecojoDocumento=?,
					otro=?,
					idNoCita=?,
					idPresupuesto=?,
					idOrdenTrabajo=?,
					idPostergacion=?,
					idEncuesta=?,
					idSeguimiento=?,
					idRecomendacion=?,
					motivoDetalle=?,
					motivoDetalleDescripcion=?`
	fecha, _ := time.Parse(time.RFC3339, this.FechaRetomarContacto)
	fechaRef1, _ := time.Parse(time.RFC3339, this.FechaCitaReferencial1)
	fechaRecdoc, _ := time.Parse(time.RFC3339, this.FechaRecojoDocumento)
	result, _ := Exec(sql,
		this.TrabajadorCorasur,
		this.TipoContactoCorasur,
		this.ContactoCorasur,
		this.Placa,
		this.IdVehiculo,
		this.Cliente,
		this.TipoContactoCliente,
		this.ContactoCliente,
		this.Activo,
		this.ResultadoContacto,
		fecha,
		this.UltimoKilometrajeServicio,
		this.FechaRegistro,
		this.IdMotivo,
		this.IdEstado,
		this.IdRespuesta,
		this.ManttoK,
		this.NuevoDuenio,
		this.TelefonoNuevoDuenio,
		this.AgendaCita,
		this.EstadoContacto,
		this.VozCliente,
		fechaRef1,
		this.DocumentoRecojo,
		fechaRecdoc,
		this.Otro,
		this.IdNoCita,
		this.IdPresupuesto,
		this.IdOrdenTrabajo,
		this.IdPostergacion,
		this.IdEncuesta,
		this.IdSeguimiento,
		this.IdRecomendacion,
		this.MotivoDetalle,
		this.MotivoDetalleDescripcion)
	this.IdContacto, _ = result.LastInsertId()
}

func (this *Contacto) actualizar() {
	sql := `INSERT x_tacontacto SET
					idContacto=?,
					trabajadorCorasur=?,
					tipoContactoCorasur=?,
					contactoCorasur=?,
					placa=?,
					idVehiculo=?,
					cliente=?,
					tipoContactoCliente=?,
					contactoCliente=?,
					activo=?,
					resultadoContacto=?,
					fechaRetomarContacto=?,
					ultimoKilometrajeServicio=?,
					fechaRegistro=?,
					idMotivo=?,
					idEstado=?,
					idRespuesta=?,
					manttoK=?,
					nuevoDuenio=?,
					telefonoNuevoDuenio=?,
					agendaCita=?,
					estadoContacto=?,
					vozCliente=?,		
					fechaCitaReferencial1=?,			
					documentoRecojo=?,
					fechaRecojoDocumento=?,
					otro=?,
					idNoCita=?,
					idPresupuesto=?,
					idOrdenTrabajo=?,
					idPostergacion=?,
					idEncuesta=?,
					idSeguimiento=?,
					idRecomendacion=?,
					motivoDetalle=?,
					motivoDetalleDescripcion=?`
	fecha, _ := time.Parse(time.RFC3339, this.FechaRetomarContacto)
	fechaRef1, _ := time.Parse(time.RFC3339, this.FechaCitaReferencial1)
	fechaRecdoc, _ := time.Parse(time.RFC3339, this.FechaRecojoDocumento)
	Exec(sql,
		this.TrabajadorCorasur,
		this.TipoContactoCorasur,
		this.ContactoCorasur,
		this.Placa,
		this.IdVehiculo,
		this.Cliente,
		this.TipoContactoCliente,
		this.ContactoCliente,
		this.Activo,
		this.ResultadoContacto,
		fecha,
		this.UltimoKilometrajeServicio,
		this.FechaRegistro,
		this.IdMotivo,
		this.IdEstado,
		this.IdRespuesta,
		this.ManttoK,
		this.NuevoDuenio,
		this.TelefonoNuevoDuenio,
		this.AgendaCita,
		this.EstadoContacto,
		this.VozCliente,
		fechaRef1,
		this.DocumentoRecojo,
		fechaRecdoc,
		this.Otro,
		this.IdNoCita,
		this.IdPresupuesto,
		this.IdOrdenTrabajo,
		this.IdPostergacion,
		this.IdEncuesta,
		this.IdSeguimiento,
		this.IdRecomendacion,
		this.MotivoDetalle,
		this.MotivoDetalleDescripcion)
}

func (this *Contacto) Delete() {
	sql := "DELETE FROM x_tacontacto WHERE placa=?"
	Exec(sql, this.Placa)
}
