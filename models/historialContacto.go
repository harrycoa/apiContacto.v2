package models

type Historial struct {
	IdContacto           int64  `json:"idContacto"`
	TrabajadorCorasur    string `json:"trabajadorCorasur"`
	TipoContactoCorasur  string `json:"tipoContactoCorasur"`
	ContactoCorasur      string `json:"contactoCorasur"`
	Placa                string `json:"placa"`
	Cliente              string `json:"cliente"`
	TipoContactoCliente  string `json:"tipoContactoCliente"`
	ContactoCliente      string `json:"contactoCliente"`
	Activo               string `json:"activo"`
	ResultadoContacto    string `json:"resultadoContacto"`
	FechaRetomarContacto string `json:"fechaRetomarContacto"`
	FechaRegistro        string `json:"fechaRegistro"`
	Motivo               string `json:"motivo"`
	Estado               string `json:"estado"`
}

type Historiales []Historial

func ContactoHistorial() Historiales {
	sql := `SELECT idcontacto,
	trabajadorCorasur,
	tipoContactoCorasur,
	contactoCorasur,
	placa,
	cliente,
	tipoContactoCliente,
	contactoCliente,
	activo, 
	resultadoContacto, 
	fechaRetomarContacto,
	fecharegistro, 	
	x_tamotivo.motivo, 
	x_taestado.estado	
	FROM x_tacontacto inner join x_tamotivo 
	on x_tacontacto.idMotivo= x_tamotivo.idMotivo inner join x_taestado 
	on x_tacontacto.idEstado = x_taestado.idEstado order by fechaRegistro desc
	LIMIT 10`
	listado := Historiales{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Historial{}
		rows.Scan(&obj.IdContacto,
			&obj.TrabajadorCorasur,
			&obj.TipoContactoCorasur,
			&obj.ContactoCorasur,
			&obj.Placa,
			&obj.Cliente,
			&obj.TipoContactoCliente,
			&obj.ContactoCliente,
			&obj.Activo,
			&obj.ResultadoContacto,
			&obj.FechaRetomarContacto,
			&obj.FechaRegistro,
			&obj.Motivo,
			&obj.Estado)
		listado = append(listado, obj)
	}
	return listado
}

func ContactoHistorialParametro(fecha1 string, fecha2 string) Historiales {
	//f1 := fecha1
	//f2 := fecha2  order by fechaRegistro desc
	//sql := "SELECT idcontacto,trabajadorCorasur,tipoContactoCorasur,contactoCorasur,placa,cliente,tipoContactoCliente,contactoCliente,activo,resultadoContacto,fechaRetomarContacto,fecharegistro,x_tamotivo.motivo,x_taestado.estado FROM x_tacontacto inner join x_tamotivo on x_tacontacto.idMotivo= x_tamotivo.idMotivo inner join x_taestado on x_tacontacto.idEstado = x_taestado.idEstado where " + fechaRegistro + " between '" + f1 + "' AND '" + f2 + "' order by fechaRegistro desc"

	sql := `SELECT idcontacto
				, trabajadorCorasur
				, tipoContactoCorasur
				, contactoCorasur
				, placa
				, cliente
				, tipoContactoCliente
				, contactoCliente
				, activo
				, resultadoContacto
				, fechaRetomarContacto
				, fecharegistro
				, x_tamotivo.motivo
				, x_taestado.estado 
			FROM x_tacontacto 
			INNER JOIN x_tamotivo 
				ON x_tacontacto.idMotivo = x_tamotivo.idMotivo 
			INNER JOIN x_taestado 
				ON x_tacontacto.idEstado = x_taestado.idEstado 
			WHERE fechaRegistro BETWEEN ? AND ?`

	listado := Historiales{}
	rows, _ := Query(sql, fecha1, fecha2)
	for rows.Next() {
		obj := Historial{}
		rows.Scan(&obj.IdContacto,
			&obj.TrabajadorCorasur,
			&obj.TipoContactoCorasur,
			&obj.ContactoCorasur,
			&obj.Placa,
			&obj.Cliente,
			&obj.TipoContactoCliente,
			&obj.ContactoCliente,
			&obj.Activo,
			&obj.ResultadoContacto,
			&obj.FechaRetomarContacto,
			&obj.FechaRegistro,
			&obj.Motivo,
			&obj.Estado)
		listado = append(listado, obj)
	}
	return listado
}
