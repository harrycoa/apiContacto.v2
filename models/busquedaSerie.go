package models

type BusquedaSerie struct {
	Serie           string `json:"serie"`
	Vin             string `json:"vin"`
	AnioFabricacion string `json:"anioFabricacion"`
	Modelo          string `json:"modelo"`
	Cliente         string `json:"cliente"`
	Telefono        string `json:"telefono"`
}

type BusquedaSeries []BusquedaSerie

func SerieBusqueda(c26 string) BusquedaSeries {
	sql := `select 	c26 as serie,  
					c7 as vin,             
					c5 as anioFabricacion,
					c3 as modelo,
					cnomclie as cliente,
					t1 as telefono					
					from tx_salidac where canudocu='N' and impreso='V' and c26=?`
	listado := BusquedaSeries{}
	rows, _ := Query(sql, c26)
	for rows.Next() {
		obj := BusquedaSerie{}
		rows.Scan(&obj.Serie, &obj.Vin, &obj.AnioFabricacion, &obj.Modelo, &obj.Cliente, &obj.Telefono)
		listado = append(listado, obj)
	}
	return listado
}
