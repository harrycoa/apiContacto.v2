package user

// Credentials credenciales de usuario
type Credentials struct {
	NombreUsuario string `json:"username"`
	ClaveUsuario  string `json:"password"`
}
