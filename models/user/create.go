package user

// Create clase usuario
type Create struct {
	NombreUsuario  string `json:"nombreUsuario"`
	NombreCompleto string `json:"nombreCompleto"`
	DNI            string `json:"dni"`
	Email          string `json:"email"`
	Estado         string `json:"estado"`
	Telefono       string `json:"telefono"`
	Direccion      string `json:"direccion"`
	ClaveUsuario   string `json:"claveUsuario"`
	ConfirmarClave string `json:"confirmarClave"`
	Rol            []int  `json:"rol"`
}
