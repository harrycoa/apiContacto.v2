package user

// User clase usuario
type User struct {
	IDUsuario     int32  `json:"idUsuario"`
	NombreUsuario string `json:"nombreUsuario"`
	// Clave   		  string `json:"claveUsuario"`
	Estado            string `json:"estado"`
	DNI               string `json:"dni"`
	NombreCompleto    string `json:"nombreCompleto"`
	Email             string `json:"email"`
	Email2            string `json:"email2"`
	Telefono          string `json:"telefono"`
	Direccion         string `json:"direccion"`
	FechaModificacion string `json:"fechaModificacion"`
	FechaRegistro     string `json:"fechaRegistro"`
	Rol               []int  `json:"rol"`
}
