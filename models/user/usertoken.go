package user

// TokenUser clase usuario para generar un token
type TokenUser struct {
	NombreUsuario  string   `json:"nombreUsuario"`
	Estado         string   `json:"estado"`
	DNI            string   `json:"dni"`
	NombreCompleto string   `json:"nombreCompleto"`
	Email          string   `json:"email"`
	Telefono       string   `json:"telefono"`
	Rol            []int `json:"rol"`
}
