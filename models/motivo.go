package models

type Motivo struct {
	IdMotivo int64  `json:"idMotivo"`
	Mclase   string `json:"mClase"`
	Motivo   string `json:"motivo"`
}

const motivoEsquema string = `CREATE TABLE x_taMotivo (
	idMotivo int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	mclase varchar(100) NOT NULL, 
	motivo varchar(100) NOT NULL
  );`

type Motivos []Motivo

// Constructores
func NuevoMotivo(mclase, motivo string) *Motivo {
	objmotivo := &Motivo{Mclase: mclase, Motivo: motivo}
	return objmotivo
}

func CrearMotivo(mclase, motivo string) *Motivo {
	objmotivo := NuevoMotivo(mclase, motivo)
	objmotivo.Guardar()
	return objmotivo
}

func ListarMotivo(mclase string) *Motivo {
	objmotivo := NuevoMotivo("", "")
	sql := "SELECT idMotivo, mclase, motivo FROM x_tamotivo where mclase=?"
	rows, _ := Query(sql, mclase)
	for rows.Next() {
		rows.Scan(&objmotivo.IdMotivo, &objmotivo.Mclase, &objmotivo.Motivo)
	}
	return objmotivo
}

func ListarMotivosParametro(mclase string) Motivos {

	sql := "SELECT idMotivo, mclase, motivo FROM x_tamotivo where mclase=?"
	listado := Motivos{}
	rows, _ := Query(sql, mclase)
	for rows.Next() {
		obj := Motivo{}
		rows.Scan(&obj.IdMotivo, &obj.Mclase, &obj.Motivo)
		listado = append(listado, obj)
	}
	return listado
}

func ListarMotivos() Motivos {
	sql := "SELECT idMotivo, mclase, motivo FROM galaxia_corasur.x_tamotivo"
	listado := Motivos{}
	rows, _ := Query(sql)

	for rows.Next() {
		obj := Motivo{}
		rows.Scan(&obj.IdMotivo, &obj.Mclase, &obj.Motivo)
		listado = append(listado, obj)
	}
	return listado
}

func (this *Motivo) Guardar() {
	if this.IdMotivo == 0 {
		this.insertar()
	} else {
		this.actualizar()
	}
}

func (this *Motivo) insertar() {
	sql := "INSERT x_taMotivo SET mclase=?, motivo=?"
	result, _ := Exec(sql, this.Mclase, this.Motivo)
	this.IdMotivo, _ = result.LastInsertId()
}

func (this *Motivo) actualizar() {
	sql := "UPDATE x_taMotivo SET mclase=?, motivo=?"
	Exec(sql, this.Mclase, this.Motivo)
}

func (this *Motivo) Delete() {
	sql := "DELETE FROM x_taMotivo WHERE idMotivo=?"
	Exec(sql, this.IdMotivo)
}
