package pruebasp

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	db "../../configuration"
	"../../results"
)

func PruebaAccesoBdNative(w http.ResponseWriter, r *http.Request) {

	// params := mux.Vars(r)
	// placa := params["placa"]
	placa := "X4H751"

	rows, err := db.SpExecQuery("CALL spPronostico5k(?)", placa)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	ordenes := OrdenReparaciones{}
	for rows.Next() {
		o := OrdenReparacion{}
		err = rows.Scan(
			&o.Placa,
			&o.Numero,
			&o.Nombre,
			&o.Km,
			&o.Telefono,
			&o.Servicio,
			&o.FechaInicio,
			&o.FechaFin,
			&o.Serie,
			&o.Motor,
			&o.Chasis,
			&o.Vin,
			&o.AnioFabricacion,
		)
		ordenes = append(ordenes, o)
		if err != nil {
			panic(err.Error())
		}
	}
	rows.Close()

	j, err := json.Marshal(ordenes)
	if err != nil {
		log.Fatalf("Error al convertir el mensaje: %s", err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
	fmt.Println("***************MYSQL NATIVE******************")
}

func PruebaAccesoBdOtherLib(w http.ResponseWriter, r *http.Request) {

	// params := mux.Vars(r)
	// placa := params["placa"]
	placa := "X4H751"

	rows, err := db.ExecQuerySp("CALL spPronostico5k(?)", placa)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	ordenes := OrdenReparaciones{}

	for _, row := range rows {
		o := OrdenReparacion{}
		o.Placa = row.Str(0)
		o.Numero = row.Int64(1)
		o.Nombre = row.Str(2)
		o.Km = row.Int64(3)
		o.Telefono = row.Str(4)
		o.Servicio = row.Str(5)
		o.FechaInicio = row.Str(6)
		o.FechaFin = row.Str(7)
		o.Serie = row.Str(8)
		o.Motor = row.Str(9)
		o.Chasis = row.Str(10)
		o.Vin = row.Str(11)
		o.AnioFabricacion = row.Str(12)

		ordenes = append(ordenes, o)
	}

	j, err := json.Marshal(ordenes)
	if err != nil {
		log.Fatalf("Error al convertir el mensaje: %s", err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
	fmt.Println("***************MYSQL OTHER LIB******************")
}
