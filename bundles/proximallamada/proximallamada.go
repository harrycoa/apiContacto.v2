package proximallamada

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"

	db "../../configuration"
	"../../results"
)

// Fecha ...
type FechaLlamada struct {
	FechaLlamada time.Time `json:"fechaLlamada"`
}

// Router ...
func Router(router *mux.Router) {
	prefix := "/api/proximallamada"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("/{placa}/{sabeKmActual}/{kilometraje}", ProximaLlamada).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}

// ProximaLlamada ...
func ProximaLlamada(w http.ResponseWriter, r *http.Request) {
	param := mux.Vars(r)
	placa := param["placa"]
	sabeKm := param["sabeKmActual"]
	kmOusoVehicular := param["kilometraje"]
	kmActual, kmDiario := 0, 0
	if sabeKm == "S" {
		var err error
		kmActual, err = strconv.Atoi(kmOusoVehicular)
		if err != nil {
			kmActual = 0
		}
	} else {
		switch kmOusoVehicular {
		case "Particular":
			kmDiario = 30
		case "Transporte":
			kmDiario = 80
		default:
			kmDiario = 55
		}
	}

	row, err := db.SpExecQueryRow("CALL spProximaLlamada(?,?,?,?)",
		placa, sabeKm, kmActual, kmDiario)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	f := FechaLlamada{}

	var d mysql.NullTime
	err = row.Scan(&d)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}
	if d.Valid {
		f.FechaLlamada = d.Time
	}

	j, err := json.Marshal(f)
	if err != nil {
		log.Fatal(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}
