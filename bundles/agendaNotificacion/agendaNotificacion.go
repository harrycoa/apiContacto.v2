package agendaNotificacion

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	db "../../configuration"
	"../../results"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

type AgendaNotificacion struct {
	Asesor              string `json:"asesor"`
	Placa               string `json:"placa"`
	Cliente             string `json:"cliente"`
	Telefono            string `json:"telefono"`
	Celular             string `json:"celular"`
	Servicio            string `json:"servicio"`
	IngresoTaller       string `json:"ingresoTaller"`
	ProgramacionEntrega string `json:"programacionEntrega"`
	AperturaOT          string `json:"aperturaOT"`
	InicioMecanico      string `json:"inicioMecanico"`
	FinMecanico         string `json:"finMecanico"`
	InicioLavado        string `json:"inicioLavado"`
	FinLavado           string `json:"finLavado"`
	FechaNotificacion   string `json:"fechaNotificacion"`
	ContestaNoContesta  string `json:"contestaNoContesta"`
	FechaEntrega        string `json:"fechaEntrega"`
	VozCliente          string `json:"vozCliente"`
}

type AgendaNotificaciones []AgendaNotificacion

// Route
func Router(router *mux.Router) {
	prefix := "/api/anotificacion"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("/{fecha}", ListAgendaNotificacion).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}

// HistorialContactoFechas ...
func ListAgendaNotificacion(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	fecha := vars["fecha"] //Esto ya es un String
	rows, err := db.SpExecQuery("CALL spAgendaNotificacion(?)", fecha)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	list := setList(rows)
	j, err := json.Marshal(list)
	if err != nil {
		log.Fatal(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func setList(rows *sql.Rows) AgendaNotificaciones {
	listado := AgendaNotificaciones{}
	for rows.Next() {
		obj := AgendaNotificacion{}
		rows.Scan(
			&obj.Asesor,
			&obj.Placa,
			&obj.Cliente,
			&obj.Telefono,
			&obj.Celular,
			&obj.Servicio,
			&obj.IngresoTaller,
			&obj.ProgramacionEntrega,
			&obj.AperturaOT,
			&obj.InicioMecanico,
			&obj.FinMecanico,
			&obj.InicioLavado,
			&obj.FinLavado,
			&obj.FechaNotificacion,
			&obj.ContestaNoContesta,
			&obj.FechaEntrega,
			&obj.VozCliente)
		listado = append(listado, obj)
	}
	return listado

}
