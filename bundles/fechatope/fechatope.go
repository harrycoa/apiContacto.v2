package fechatope

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"

	db "../../configuration"
	"../../results"
)

// Fecha ...
type Fecha struct {
	FechaLimite time.Time `json:"fechaLimite"`
}

// Router ...
func Router(router *mux.Router) {
	prefix := "/api/fechalimite/{placa}"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", FechaLimiteLlamada).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}

// FechaLimiteLlamada ...
func FechaLimiteLlamada(w http.ResponseWriter, r *http.Request) {

	param := mux.Vars(r)
	placa := param["placa"]

	row, err := db.SpExecQueryRow("CALL spFechaLimiteLlamada(?)", placa)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	f := Fecha{}

	err = row.Scan(&f.FechaLimite)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	j, err := json.Marshal(f)
	if err != nil {
		results.ErrorResult(w, err.Error())
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}
