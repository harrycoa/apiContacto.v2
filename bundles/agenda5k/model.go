package agenda5k

type Agenda5k struct {
	IdPronostico5k             int64   `json:"idPronostico5k"`
	Placa                      string  `json:"placa"`
	Modelo                     string  `json:"modelo"`
	Cliente                    string  `json:"cliente"`
	Telefono1                  string  `json:"telefono1"`
	Telefono2                  string  `json:"telefono2"`
	Mantenimiento              string  `json:"mantenimiento"`
	Prioridad                  string  `json:"prioridad"`
	Fecha15Antes               string  `json:"fecha15Antes"`
	FechaPactadaUltimoContacto string  `json:"fechaPactadaUltimoContacto"`
	FechaCorregida             string  `json:"fechaCorregida"`
	FechaCorregidaSistema      string  `json:"fechaCorregidaSistema"`
	FechaWppEnvio              string  `json:"fechaWppEnvio"`
	VistoBueno                 string  `json:"vistoBueno"`
	Resultado                  string  `json:"resultado"`
	FechaLlamadaEfectuada      string  `json:"fechaLlamadaEfectuada"`
	Contesta                   string  `json:"contesta"`
	Resultado2                 string  `json:"resultado2"`
	FechaPactadaVolverLlamar   string  `json:"fechaPactadaVolverLlamar"`
	FechaCita                  string  `json:"fechaCita"`
	NroPrepuesto               string  `json:"nroPresupuesto"`
	Recomendacion              string  `json:"recomendacion"`
	Costo                      float64 `json:"costo"`
}

// Agenda5ks ...
type Agenda5ks []Agenda5k
