package agenda5k

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	db "../../configuration"
	"../../results"
)

// ListarAgenda5k ...
func ListarAgenda5k(w http.ResponseWriter, r *http.Request) {

	rows, err := db.SpExecQuery("CALL spListarAgenda(?,?)", 0, "20183011")
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	list := setList(rows)
	rows.Close()

	j, err := json.Marshal(list)
	if err != nil {
		log.Fatal(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func setList(rows *sql.Rows) Agenda5ks {
	listado := Agenda5ks{}
	for rows.Next() {
		obj := Agenda5k{}
		rows.Scan(
			&obj.IdPronostico5k,
			&obj.Placa,
			&obj.Modelo,
			&obj.Cliente,
			&obj.Telefono1,
			&obj.Telefono2,
			&obj.Mantenimiento,
			&obj.Prioridad,
			&obj.Fecha15Antes,
			&obj.FechaPactadaUltimoContacto,
			&obj.FechaCorregida,
			&obj.FechaCorregidaSistema,
			&obj.FechaWppEnvio,
			&obj.VistoBueno,
			&obj.Resultado,
			&obj.FechaLlamadaEfectuada,
			&obj.Contesta,
			&obj.Resultado2,
			&obj.FechaPactadaVolverLlamar,
			&obj.FechaCita,
			&obj.NroPrepuesto,
			&obj.Recomendacion,
			&obj.Costo)
		listado = append(listado, obj)
	}
	return listado

}

// ListarAgenda5kParametro ...
// func ListarAgenda5kParametro(w http.ResponseWriter, r *http.Request) {
// 	models.CreateConnection()
// 	vars := mux.Vars(r)
// 	fecha1 := vars["fecha1"] //Esto ya es un String
// 	fecha2 := vars["fecha2"] //Esto ya es un String

// 	// fmt.Println("===============FECHA INICIAL=====================")
// 	// fmt.Println(fecha1)
// 	// fmt.Println("============FECHA FINAL========================")
// 	// fmt.Println(fecha2)

// 	res := models.ListarAgenda5kParametro(fecha1, fecha2)
// 	j, err := json.Marshal(res)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	w.Write(j)
// 	w.WriteHeader(http.StatusOK)
// 	models.Closeconnection()
// }
