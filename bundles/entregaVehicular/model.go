package entregaVehicular

type EntregaVehicular struct {
	IdEntregaVehicular       int64  `json:"idEntregaVehicular"`
	Serie                    string `json:"serie"`
	Placa                    string `json:"placa"`
	Motor                    string `json:"motor"`
	Chasis                   string `json:"chasis"`
	Vin                      string `json:"vin"`
	AnioFabricacion          string `json:"anioFabricacion"`
	Modelo                   string `json:"modelo"`
	FechaProgramacionEntrega string `json:"fechaProgramacionEntrega"`
	FechaEntregaVehiculo     string `json:"fechaEntregaVehiculo"`
	PersonaEntrega           string `json:"personaEntrega"`
	Telefono                 string `json:"telefono"`
	Celular                  string `json:"celular"`
	OpinionEntrega           string `json:"opinionEntrega"`
	FechaCitaReferencial2    string `json:"fechaCitaReferencial2"`
	UsoVehicular             string `json:"usoVehicular"`
	IdDepartamento           int64  `json:"idDepartamento"`
	IdProvincia              int64  `json:"idProvincia"`
}

type EntregasVehiculares []EntregaVehicular
