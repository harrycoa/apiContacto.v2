package historialContacto

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	db "../../configuration"
	"../../results"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

type HistorialContacto struct {
	Placa             string `json:"placa"`
	FechaRegistro     string `json:"fechaRegistro"`
	Cliente           string `json:"cliente"`
	ContactoCliente   string `json:"contactoCliente"`
	Estado            string `json:"estado"`
	Motivo            string `json:"motivo"`
	SeLogroContacto   string `json:"seLogroContacto"`
	VozCliente        string `json:"vozCliente"`
	TrabajadorCorasur string `json:"trabajadorCorasur"`
}

type HistorialContactos []HistorialContacto

// Route
func Router(router *mux.Router) {
	prefix := "/api/historial"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", ListHistorialContacto).Methods("GET")
	subrouter.HandleFunc("/{fecha1}/{fecha2}", ListHistorialContactoFechas).Methods("GET")
	subrouter.HandleFunc("/{placa}", ListHistorialContactoPlaca).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}

// HistorialContacto ...
func ListHistorialContacto(w http.ResponseWriter, r *http.Request) {

	rows, err := db.SpExecQuery("SELECT * FROM v_historialcontacto")
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	list := setList(rows)
	rows.Close()

	j, err := json.Marshal(list)
	if err != nil {
		log.Fatal(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

// HistorialContactoFechas ...
func ListHistorialContactoFechas(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	fecha1 := vars["fecha1"] //Esto ya es un String
	fecha2 := vars["fecha2"] //Esto ya es un String

	rows, err := db.SpExecQuery("CALL spListarHistorialFechas(?,?)", fecha1, fecha2)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	list := setList(rows)

	j, err := json.Marshal(list)
	if err != nil {
		log.Fatal(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

// HistorialContactoPlaca ...
func ListHistorialContactoPlaca(w http.ResponseWriter, r *http.Request) {
	param := mux.Vars(r)
	placa := param["placa"]

	rows, err := db.SpExecQuery("CALL spListarHistorialPlaca(?)", placa)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	list := setList(rows)

	j, err := json.Marshal(list)
	if err != nil {
		log.Fatal(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func setList(rows *sql.Rows) HistorialContactos {
	listado := HistorialContactos{}
	for rows.Next() {
		obj := HistorialContacto{}
		rows.Scan(
			&obj.Placa,
			&obj.FechaRegistro,
			&obj.Cliente,
			&obj.ContactoCliente,
			&obj.Estado,
			&obj.Motivo,
			&obj.SeLogroContacto,
			&obj.VozCliente,
			&obj.TrabajadorCorasur)
		listado = append(listado, obj)
	}
	return listado

}
