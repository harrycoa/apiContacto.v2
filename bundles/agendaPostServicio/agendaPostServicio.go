package agendaPostServicio

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	db "../../configuration"
	"../../results"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

type AgendaPostServicio struct {
	Nro                   string `json:"nro"`
	Placa                 string `json:"placa"`
	Nombre                string `json:"nombre"`
	Telefono              string `json:"telefono"`
	Celular               string `json:"celular"`
	RecomendacionMecanico string `json:"recomendacionMecanico"`
	RecomendacionAsesor   string `json:"recomendacionAsesor"`
	Recepcion             string `json:"recepcion"`
	NotificacionTT        string `json:"notificacionTT"`
	NotificacionHPE       string `json:"notificacionHPE"`
	CierreOT              string `json:"cierreOT"`
	FacturacionOT         string `json:"facturacionOT"`
	P1                    string `json:"p1"`
	P2                    string `json:"p2"`
	P3                    string `json:"p3"`
	VozCliente            string `json:"vozCliente"`
}

type AgendaPostServicios []AgendaPostServicio

// Route
func Router(router *mux.Router) {
	prefix := "/api/apostservicio"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("/{servicio}/{fecha}", ListAgendaPostServicio).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}

// HistorialContactoFechas ...
func ListAgendaPostServicio(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	servicio := vars["servicio"] //Esto ya es un String
	fecha := vars["fecha"]       //Esto ya es un String
	rows, err := db.SpExecQuery("CALL spAgendaPostServicio(?,?)", servicio, fecha)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	list := setList(rows)
	j, err := json.Marshal(list)
	if err != nil {
		log.Fatal(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func setList(rows *sql.Rows) AgendaPostServicios {
	listado := AgendaPostServicios{}
	for rows.Next() {
		obj := AgendaPostServicio{}
		rows.Scan(
			&obj.Nro,
			&obj.Placa,
			&obj.Nombre,
			&obj.Telefono,
			&obj.Celular,
			&obj.RecomendacionMecanico,
			&obj.RecomendacionAsesor,
			&obj.Recepcion,
			&obj.NotificacionTT,
			&obj.NotificacionHPE,
			&obj.CierreOT,
			&obj.FacturacionOT,
			&obj.P1,
			&obj.P2,
			&obj.P3,
			&obj.VozCliente)
		listado = append(listado, obj)
	}
	return listado

}
