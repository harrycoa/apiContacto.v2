package formulario

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	db "../../configuration"

	"../../results"
)

// InsertarForm ...
func InsertarForm(w http.ResponseWriter, r *http.Request) {
	contacto := FormularioContacto{}

	err := json.NewDecoder(r.Body).Decode(&contacto)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	row, err := guardar(contacto)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	id := 0
	me := "nada"
	err = row.Scan(&id, &me)
	fmt.Println(id)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}
	fmt.Println(id)

	detalle := []ContactoDetalle{}
	detalle = contacto.ContactoDetalle

	for i := range detalle {
		row, err = guardarDetalle(id, detalle[i])
	}
	fmt.Println(id)
	results.CreatedResult(w, "Registro Exitoso!")
}

func guardarDetalle(idContacto int, d ContactoDetalle) (*sql.Row, error) {
	spName := db.SpName("spInsertarContactoDetalle", 4)
	row, err := db.SpExecQueryRow(spName,
		d.Detalle,
		db.IsStr(d.DetalleDescripcion),
		db.IsStr(d.Estado),
		idContacto)
	return row, err
}

func guardar(c FormularioContacto) (*sql.Row, error) {
	spName := db.SpName("spInsertarContacto", 53)
	row, err := db.SpExecQueryRow(spName,
		db.IsStr(c.TrabajadorCorasur),
		db.IsStr(c.TipoContactoCorasur),
		db.IsStr(c.ContactoCorasur),
		c.Placa,
		db.IsStr(c.Cliente),
		db.IsStr(c.TipoContactoCliente),
		db.IsStr(c.ContactoCliente),
		db.IsStr(c.SeLogroContacto),
		db.IsDate(c.FechaRetomarContacto),
		c.UltimoKilometrajeServicio,
		c.IdMotivo,
		c.IdEstado,
		c.ManttoK,
		db.IsStr(c.NuevoDuenio),
		db.IsStr(c.TelefonoNuevoDuenio),
		db.IsStr(c.Serie),
		db.IsStr(c.Motor),
		db.IsStr(c.Chasis),
		db.IsStr(c.Vin),
		db.IsStr(c.AnioFabricacion),
		db.IsStr(c.Modelo),
		db.IsStr(c.MotivoDetalle),
		db.IsStr(c.MotivoDescripcion),
		db.IsStr(c.EstadoContacto),
		db.IsStr(c.VozCliente),
		db.IsDate(c.FechaRecojoDocumento),
		db.IsStr(c.AgendaCita),
		db.IsDate(c.FechaCita),
		db.IsDate(c.FechaCitaCambiada),
		db.IsStr(c.PersonaModificaCita),
		db.IsStr(c.RazonModificaCita),
		db.IsStr(c.HoraPosibleArribo),
		db.IsStr(c.EstadoReprogramar),
		db.IsStr(c.HoraInicio),
		db.IsStr(c.HoraEntrega),
		db.IsDate(c.FechaCitaReprogramada),
		db.IsStr(c.DetalleCita),
		db.IsStr(c.EstadoCita),
		db.IsStr(c.ResumenContacto),
		db.IsStr(c.SabeKm),
		c.KmActual,
		db.IsStr(c.UsoVehicular),
		db.IsDate(c.FechaLimiteLlamada),
		db.IsDate(c.FechaPronosticoProxLlamada),
		db.IsDate(c.FechaSugeridaClienteProxLlamada),
		db.IsDate(c.FechaCorregidaSistemaProxLlamada),
		db.IsDate(c.FechaPactadaProxLlamada),
		db.IsStr(c.RazonNoCita),
		db.IsStr(c.NumeroOt),
		db.IsStr(c.AFreno),
		db.IsStr(c.BFreno),
		db.IsStr(c.CFreno),
		db.IsStr(c.DFreno),
	)
	return row, err
}
