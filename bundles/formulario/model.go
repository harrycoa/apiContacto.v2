package formulario

type FormularioContacto struct {
	IdContacto                       int64             `json:"idContacto"`
	TrabajadorCorasur                string            `json:"trabajadorCorasur"`
	TipoContactoCorasur              string            `json:"tipoContactoCorasur"`
	ContactoCorasur                  string            `json:"contactoCorasur"`
	Placa                            string            `json:"placa"`
	Cliente                          string            `json:"cliente"`
	TipoContactoCliente              string            `json:"tipoContactoCliente"`
	ContactoCliente                  string            `json:"contactoCliente"`
	SeLogroContacto                  string            `json:"seLogroContacto"`
	FechaRetomarContacto             string            `json:"fechaRetomarContacto"`
	UltimoKilometrajeServicio        int64             `json:"ultimoKilometrajeServicio"`
	IdMotivo                         int64             `json:"idMotivo"`
	IdEstado                         int64             `json:"idEstado"`
	ManttoK                          int64             `json:"manttoK"`
	NuevoDuenio                      string            `json:"nuevoDuenio"`
	TelefonoNuevoDuenio              string            `json:"telefonoNuevoDuenio"`
	Serie                            string            `json:"serie"`
	Motor                            string            `json:"motor"`
	Chasis                           string            `json:"chasis"`
	Vin                              string            `json:"vin"`
	AnioFabricacion                  string            `json:"anioFabricacion"`
	Modelo                           string            `json:"modelo"`
	MotivoDetalle                    string            `json:"motivoDetalle"`
	MotivoDescripcion                string            `json:"motivoDescripcion"`
	EstadoContacto                   string            `json:"estadoContacto"`
	VozCliente                       string            `json:"vozCliente"`
	FechaRecojoDocumento             string            `json:"fechaRecojoDocumento"`
	AgendaCita                       string            `json:"agendaCita"`
	FechaCita                        string            `json:"fechaCita"`
	FechaCitaCambiada                string            `json:"fechaCitaCambiada"`
	PersonaModificaCita              string            `json:"personaModificaCita"`
	RazonModificaCita                string            `json:"razonModificaCita"`
	HoraPosibleArribo                string            `json:"horaPosibleArribo"`
	EstadoReprogramar                string            `json:"estadoReprogramar"`
	HoraInicio                       string            `json:"horaInicio"`
	HoraEntrega                      string            `json:"horaEntrega"`
	FechaCitaReprogramada            string            `json:"fechaCitaReprogramada"`
	DetalleCita                      string            `json:"detalleCita"`
	EstadoCita                       string            `json:"estadoCita"`
	ResumenContacto                  string            `json:"resumenContacto"`
	SabeKm                           string            `json:"sabeKm"`
	KmActual                         int64             `json:"kmActual"`
	UsoVehicular                     string            `json:"usoVehicular"`
	FechaLimiteLlamada               string            `json:"fechaLimiteLlamada"`
	FechaPronosticoProxLlamada       string            `json:"fechaPronosticoProxLlamada"`
	FechaSugeridaClienteProxLlamada  string            `json:"fechaSugeridaClienteProxLlamada"`
	FechaCorregidaSistemaProxLlamada string            `json:"fechaCorregidaSistemaProxLlamada"`
	FechaPactadaProxLlamada          string            `json:"fechaPactadaProxLlamada"`
	RazonNoCita                      string            `json:"razonNoCita"`
	NumeroOt                         string            `json:"numeroOt"`
	AFreno                           string            `json:"aFreno"`
	BFreno                           string            `json:"bFreno"`
	CFreno                           string            `json:"cFreno"`
	DFreno                           string            `json:"dFreno"`
	ContactoDetalle                  []ContactoDetalle `json:"detalle"`
}

type ContactoDetalle struct {
	IdContactoDetalle  int64  `json:"idContactoDetalle"`
	Detalle            string `json:"detalle"`
	DetalleDescripcion string `json:"detalleDescripcion"`
	Estado             string `json:"estado"`
	IdContacto         int64  `json:"idContacto"`
}

type FormularioContactos []FormularioContacto
type ContactoDetalles []ContactoDetalle
