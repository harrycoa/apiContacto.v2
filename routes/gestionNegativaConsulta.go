package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func GestionNegativaConsultaRouter(router *mux.Router) {
	prefix := "/api/gestion"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.ListarGestionConsultas).Methods("GET")
	subrouter.HandleFunc("/{placa}", controllers.ListarGestionConsulta).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
