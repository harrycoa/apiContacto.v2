package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func RazonNoCitaRouter(router *mux.Router) {
	prefix := "/api/razon"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.ListarRazonNoCitas).Methods("GET")
	subrouter.HandleFunc("/{idMotivo}", controllers.ListarRazonNoCita).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
