package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func ListarAgenda5kRouter(router *mux.Router) {
	prefix := "/api/agenda5k"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.ListarAgenda5k).Methods("GET")
	subrouter.HandleFunc("/{fecha1}/{fecha2}", controllers.ListarAgenda5kParametro).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
