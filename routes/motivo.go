package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// SetMotivoRouter ruta de motivo
func MotivosRouter(router *mux.Router) {
	prefix := "/api/motivo"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.ListarMotivos).Methods("GET")
	subrouter.HandleFunc("/{mclase}", controllers.ListarMotivoParametro).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
