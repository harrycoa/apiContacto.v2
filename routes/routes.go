package routes

import (
	"../bundles/agenda5k"
	"../bundles/agendaNotificacion"
	"../bundles/agendaPostServicio"
	"../bundles/busquedaVehicular"
	"../bundles/entregaVehicular"
	"../bundles/fechatope"
	"../bundles/formulario"
	"../bundles/historialContacto"
	"../bundles/proximallamada"
	"../bundles/pruebaEntrega"
	sp "../bundles/pruebasp"

	"github.com/gorilla/mux"
)

// InitRoutes ...
func InitRoutes() *mux.Router {
	router := mux.NewRouter().StrictSlash(false)
	SetLoginRouter(router)
	SetUserRouter(router)
	// estados
	EstadosRouter(router)
	// motivos
	MotivosRouter(router)
	// contacto
	ContactoRouter(router)
	// respuesta
	RespuestasRouter(router)
	// orden
	OrdenRouter(router)
	// no cita
	NoCitaRouter(router)
	// cita
	CitaRouter(router)
	// uso vehicular
	// UsoVehicularRouter(router)
	// modelo vehicular
	// ModeloVehicularRouter(router)
	// departamento
	DepartamentoRouter(router)
	//provincia
	ProvinciaRouter(router)
	// Razon No cita
	RazonNoCitaRouter(router)
	// Razon Si cita
	//RazonSiCitaRouter(router)
	// Motivo Resultado
	MotivoResultadoRouter(router)
	// servicio
	ServicioRouter(router)
	// presupuesto
	PresupuestoRouter(router)
	// vehiculo
	VehiculoRouter(router)
	// consulta vehicular
	ConsultaVehicularRouter(router)
	// formulario
	InsertarFormularioRouter(router)
	// formulario Entrega
	InsertarFormularioEntregaRouter(router)
	// Gestion Negativa Consulta
	GestionNegativaConsultaRouter(router)
	// Busqueda serie
	BusquedaSerieRouter(router)
	// Busqueda Placa
	BusquedaPlacaRouter(router)
	// gestion negativa
	GestionNegativaRouter(router)
	//
	HistorialRouter(router)
	// listar vehiculo
	ListaVehiculoRouter(router)
	// Historial modifi
	HistorialContRouter(router)
	//agenda 5k
	// ListarAgenda5kRouter(router)

	// ROUTER REFACTOR
	fechatope.Router(router)
	proximallamada.Router(router)
	agenda5k.Router(router)
	formulario.Router(router)

	pruebaEntrega.Router(router)

	// Busqueda Vehicular
	busquedaVehicular.Router(router)

	// EntregaVehicular
	entregaVehicular.EntregaRouter(router)
	entregaVehicular.BusquedaEntregaRouter(router)

	// Historial
	historialContacto.Router(router)

	agendaNotificacion.Router(router)

	agendaPostServicio.Router(router)

	// PRUEBAS
	PruebasRouter(router)
	sp.PruebaAccesoBdRouter(router)

	return router
}
