package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func ServicioRouter(router *mux.Router) {
	prefix := "/api/servicio"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.InsertarServicio).Methods("POST")
	subrouter.HandleFunc("", controllers.ListarServicios).Methods("GET")
	subrouter.HandleFunc("/{serie}", controllers.ListarServicio).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
