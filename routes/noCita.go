package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func NoCitaRouter(router *mux.Router) {
	prefix := "/api/nocita"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.InsertarNoCita).Methods("POST")
	subrouter.HandleFunc("", controllers.ActualizarNoCita).Methods("PUT")
	subrouter.HandleFunc("", controllers.EliminarRazon).Methods("DELETE")
	subrouter.HandleFunc("", controllers.ListarNoCitas).Methods("GET")
	subrouter.HandleFunc("/{claseRazon}", controllers.ListarNoCitasParametro).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
